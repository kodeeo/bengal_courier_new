<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', static function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->string('sender_name');
            $table->string('sender_phone');
            $table->text('sender_address');
            $table->text('sender_email')->nullable();
            $table->string('receiver_name');
            $table->string('receiver_phone');
            $table->text('receiver_address')->nullable();
            $table->string('receiver_email')->nullable();
            $table->string('tracking_number');
            $table->unsignedBigInteger('location_id')->nullable()
                ->comment('form-location');
            $table->string('status', 16);
            $table->timestamp('collection_date_time')->nullable();
            $table->timestamp('delivery_date_time')->nullable();
            $table->string('payment_status')->nullable();
            $table->decimal('weight');
            $table->text('description')->nullable();
            $table->text('note')->nullable();
            $table->decimal('deliver_charge', 11, 5)->nullable();
            $table->unsignedBigInteger('product_type')->nullable();
            $table->unsignedBigInteger('service_mode')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->unsignedBigInteger('created_by')->nullable();
            $table->decimal('quantity');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
