<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create ('locations', static function (Blueprint $table) {
            $table->bigIncrements ('id');
            $table->string ('office_name');
            $table->text ('address')->nullable ();
            $table->string ('phone');
            $table->string ('city');
            $table->string ('office_timing');
            $table->string ('contact_person');
            $table->boolean ('status');
            $table->timestamps ();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists ('locations');
    }
}
