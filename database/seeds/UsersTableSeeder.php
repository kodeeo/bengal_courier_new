<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::create([
            'name' => 'Admin',
            'username' => 'admin',
            'email' => 'admin@gmail.com',
            'email_verified_at' => now(),
            'password' => bcrypt('admin'), // admin
            'remember_token' => Str::random(10),
            'role' => 'admin',
            'phone' => '017701461789',
            'status' => 1,
            'country' => 'Bangladesh',
            'zipcode' => '13434',
            'office' => 'banani',
            'state' => 'Dhaka',
            'company' => 'Bengal Courier'
        ]);
    }
}
