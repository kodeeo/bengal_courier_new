<?php

use Illuminate\Database\Seeder;

class companySettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Setting::create([
            'company_name'=>'Bengale Courier',
            'company_phone'=>'0134998499',
            'email'=>'bengalecourie@info.com',
            'country'=>'Bangladesh',
            'city'=>'Dhaka',
            'address'=>'Dhaka,Bangladesh',
            'logo'=>'logo.png'
        ]);
    }
}
