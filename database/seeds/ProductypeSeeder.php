<?php

use Illuminate\Database\Seeder;

class ProductypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Models\ProductType::class, 7)->create();
    }
}
