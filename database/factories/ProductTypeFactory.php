<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Models\ProductType::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'type_of_packing' => $faker->word(),
        'dimension' => $faker->randomElement(['23*45', '34*54']),
        'status' => $faker->boolean
    ];
});
