<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define (\App\Models\ShippingType::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'services' => $faker->name,
        'delivery' => $faker->name,
        'observation' => $faker->word (),
        'status' => $faker->boolean
    ];
});
