-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 12, 2020 at 11:35 AM
-- Server version: 5.7.24
-- PHP Version: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `parcel`
--

-- --------------------------------------------------------

--
-- Table structure for table `company_settings`
--

CREATE TABLE `company_settings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `company_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `per_kg_price` decimal(8,2) NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `currency` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `currency_symbol` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `footer_text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `locations`
--

CREATE TABLE `locations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `office_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `office_timing` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_person` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_02_09_045052_create_orders_table', 1),
(5, '2020_02_09_054654_create_shipping_types_table', 1),
(6, '2020_02_09_054923_create_product_types_table', 1),
(7, '2020_02_10_064737_create_locations_table', 1),
(8, '2020_02_10_083718_create_company_settings_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `sender_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sender_phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sender_address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `receiver_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `receiver_phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `receiver_address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `receiver_email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tracking_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location_id` bigint(20) UNSIGNED NOT NULL COMMENT 'form-location',
  `destination` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(16) COLLATE utf8mb4_unicode_ci NOT NULL,
  `collection_date_time` timestamp NOT NULL,
  `delivery_date_time` timestamp NOT NULL,
  `payment_status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `freight_price` decimal(8,2) NOT NULL,
  `subtotal` decimal(8,2) NOT NULL,
  `total` decimal(20,2) NOT NULL,
  `weight` decimal(8,2) NOT NULL,
  `per_kg_price` decimal(8,2) NOT NULL,
  `product_type` bigint(20) UNSIGNED NOT NULL,
  `service_mode` bigint(20) UNSIGNED NOT NULL,
  `quantity` int(11) NOT NULL,
  `create_by` bigint(20) UNSIGNED DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_types`
--

CREATE TABLE `product_types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type_of_packing` text COLLATE utf8mb4_unicode_ci,
  `dimension` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_types`
--

INSERT INTO `product_types` (`id`, `name`, `type_of_packing`, `dimension`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Elvie Ullrich', 'nulla', '23*45', 1, '2020-02-12 05:34:57', '2020-02-12 05:34:57'),
(2, 'Janelle Jerde', 'cupiditate', '23*45', 1, '2020-02-12 05:34:57', '2020-02-12 05:34:57'),
(3, 'Prof. Micheal Becker', 'iusto', '23*45', 0, '2020-02-12 05:34:57', '2020-02-12 05:34:57'),
(4, 'Amani Weissnat', 'ipsam', '34*54', 1, '2020-02-12 05:34:57', '2020-02-12 05:34:57'),
(5, 'Anabel Predovic', 'perferendis', '34*54', 1, '2020-02-12 05:34:57', '2020-02-12 05:34:57'),
(6, 'Cody Hills', 'excepturi', '23*45', 0, '2020-02-12 05:34:57', '2020-02-12 05:34:57'),
(7, 'Carissa Hermiston', 'autem', '23*45', 0, '2020-02-12 05:34:57', '2020-02-12 05:34:57');

-- --------------------------------------------------------

--
-- Table structure for table `shipping_types`
--

CREATE TABLE `shipping_types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `services` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `delivery` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `observation` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `shipping_types`
--

INSERT INTO `shipping_types` (`id`, `name`, `services`, `delivery`, `observation`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Stan Stoltenberg', 'Hillard Emard DVM', 'Prof. Isaiah Lakin II', 'omnis', 0, '2020-02-12 05:34:57', '2020-02-12 05:34:57'),
(2, 'Stephanie Lockman DVM', 'Prof. Garfield DuBuque', 'Perry Hoeger', 'rerum', 0, '2020-02-12 05:34:57', '2020-02-12 05:34:57'),
(3, 'Jeramy Heaney Jr.', 'Mr. Herminio Jones PhD', 'Dr. Scotty Morar Sr.', 'quo', 1, '2020-02-12 05:34:57', '2020-02-12 05:34:57'),
(4, 'Francesco Ortiz', 'Orpha Runolfsson', 'Deangelo Weber', 'sed', 0, '2020-02-12 05:34:57', '2020-02-12 05:34:57'),
(5, 'Bridgette Hodkiewicz', 'Nedra Runte DDS', 'Karine Gerhold PhD', 'quia', 1, '2020-02-12 05:34:57', '2020-02-12 05:34:57'),
(6, 'Mozell Ebert', 'Cassidy Bernier', 'Phoebe Strosin', 'reiciendis', 1, '2020-02-12 05:34:57', '2020-02-12 05:34:57'),
(7, 'Carlotta Reynolds', 'Mrs. Stella Eichmann Sr.', 'Saul Bernhard III', 'nesciunt', 1, '2020-02-12 05:34:57', '2020-02-12 05:34:57'),
(8, 'Napoleon Lindgren', 'Archibald Dicki', 'Wilton Predovic', 'iusto', 1, '2020-02-12 05:34:57', '2020-02-12 05:34:57'),
(9, 'Miss Maryjane Romaguera Sr.', 'Emmitt Nicolas', 'Lura Jaskolski', 'porro', 1, '2020-02-12 05:34:57', '2020-02-12 05:34:57'),
(10, 'Prof. Kadin Hettinger', 'Kevin Ziemann PhD', 'Earline Leuschke', 'sed', 1, '2020-02-12 05:34:57', '2020-02-12 05:34:57');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'customer',
  `office` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `address` text COLLATE utf8mb4_unicode_ci,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zipcode` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `account_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `email`, `email_verified_at`, `password`, `role`, `office`, `phone`, `status`, `address`, `country`, `state`, `company`, `zipcode`, `account_type`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Sohel', 'sohel', 'admin@gmail.com', '2020-02-12 05:34:56', '$2y$10$J9VDQ7hr6n1kQ0AVa15GQuSCx9R9iTTSx6mhI561sjrkAJKmTMlji', 'admin', 'banani', '017701461789', 1, NULL, 'Bangladesh', 'Dhaka', 'test', '13434', NULL, 'ewV22r4vR5', '2020-02-12 05:34:56', '2020-02-12 05:34:56'),
(2, 'Prof. Katlyn McLaughlin', 'Maudie Watsica', 'maymie.vonrueden@example.com', '2020-02-12 05:34:57', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'customer', 'Pollich Group', '(208) 550-5605', 0, NULL, 'Congo', 'Texas', 'Brekke Group', '02061-1888', NULL, '4tuOzCbDwH', '2020-02-12 05:34:57', '2020-02-12 05:34:57'),
(3, 'Anabelle Collier III', 'Adan Mosciski PhD', 'barrows.rhiannon@example.org', '2020-02-12 05:34:57', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'employee', 'Gaylord, Little and Douglas', '(493) 372-3094 x105', 1, NULL, 'Cayman Islands', 'North Carolina', 'Gulgowski-Metz', '56448-3026', NULL, 'xFe5tE8KUF', '2020-02-12 05:34:57', '2020-02-12 05:34:57'),
(4, 'Fabian Ullrich', 'Mrs. Annabelle Bechtelar', 'toni29@example.com', '2020-02-12 05:34:57', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'customer', 'O\'Keefe-Douglas', '1-589-616-0788', 0, NULL, 'Macedonia', 'Massachusetts', 'Morissette and Sons', '47315', NULL, 'ERVL8KnbbS', '2020-02-12 05:34:57', '2020-02-12 05:34:57'),
(5, 'Princess Streich', 'Lisette Kling', 'erdman.ashleigh@example.com', '2020-02-12 05:34:57', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'customer', 'Greenfelder, Cruickshank and O\'Reilly', '+1-850-778-5487', 1, NULL, 'Tajikistan', 'Nebraska', 'Rippin, Bayer and Hartmann', '85820', NULL, 'YdAMg1WHbU', '2020-02-12 05:34:57', '2020-02-12 05:34:57'),
(6, 'Clemens Schneider', 'Roma Kub', 'elisa.hamill@example.com', '2020-02-12 05:34:57', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'employee', 'Collier, Turner and Lesch', '(649) 398-7993 x373', 1, NULL, 'Slovenia', 'Georgia', 'Kuhic PLC', '64479', NULL, 'iANPIRZe3z', '2020-02-12 05:34:57', '2020-02-12 05:34:57'),
(7, 'Lizzie Shields', 'Jamarcus Swaniawski', 'leuschke.skyla@example.com', '2020-02-12 05:34:57', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'admin', 'Swaniawski PLC', '645-603-8794 x213', 1, NULL, 'Gabon', 'Utah', 'Conn, Stanton and Carter', '82966', NULL, '8PbimQD3p1', '2020-02-12 05:34:57', '2020-02-12 05:34:57'),
(8, 'Lessie Kihn', 'Prof. Ramon Kris MD', 'hans72@example.org', '2020-02-12 05:34:57', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'employee', 'Walsh-Cassin', '(816) 574-6780', 1, NULL, 'Saudi Arabia', 'Kentucky', 'Kuhic, Welch and Jast', '69337-0528', NULL, '3LijxwDnUP', '2020-02-12 05:34:57', '2020-02-12 05:34:57'),
(9, 'Eliza Leuschke Sr.', 'Dr. Coy Fritsch', 'okonopelski@example.org', '2020-02-12 05:34:57', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'admin', 'Frami Ltd', '+1-334-382-7214', 0, NULL, 'Russian Federation', 'New Jersey', 'Gutkowski and Sons', '35637-5425', NULL, 'HmTsiMigT3', '2020-02-12 05:34:57', '2020-02-12 05:34:57'),
(10, 'Bo Kiehn', 'Mr. Clovis Graham', 'freida.baumbach@example.org', '2020-02-12 05:34:57', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'admin', 'Renner, Moen and Predovic', '898.463.6169 x839', 1, NULL, 'Greece', 'Colorado', 'Tillman and Sons', '36826-6803', NULL, 'EC4bNj0tiM', '2020-02-12 05:34:57', '2020-02-12 05:34:57'),
(11, 'Reina Emmerich', 'Verdie Pollich', 'nayeli29@example.com', '2020-02-12 05:34:57', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'employee', 'Breitenberg-Kirlin', '+15652555310', 0, NULL, 'South Africa', 'Arizona', 'Volkman-Botsford', '35149', NULL, 'iQX1ypFHNJ', '2020-02-12 05:34:57', '2020-02-12 05:34:57');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `company_settings`
--
ALTER TABLE `company_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `locations`
--
ALTER TABLE `locations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `product_types`
--
ALTER TABLE `product_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shipping_types`
--
ALTER TABLE `shipping_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `company_settings`
--
ALTER TABLE `company_settings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `locations`
--
ALTER TABLE `locations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_types`
--
ALTER TABLE `product_types`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `shipping_types`
--
ALTER TABLE `shipping_types`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
