<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class Order extends Model
{

    public const  PENDING = 'pending';

    public const  RECEIEVED = 'received';

    public const COLLECTING = 'collecting';

    public const DELIVERD = 'deliverd';

    public const REDELIVERD = 'redeliverd';

    public const RETURNT = 'return';

    protected $guarded = [];
    protected $with = ['productType'];


    protected  $casts = [
        'delivery_date_time'=>'datetime'
    ];

    public function getDeliveryDateAttribute( )
    {
        return (new Carbon($this->delivery_date_time))->format('Y-m-d');
    }
    public function getCollectDateAttribute( )
    {
        return (new Carbon($this->delivery_date_time))->format('Y-m-d');
    }
    public static function getNextInvoiceNumber()
    {
        // Get the last created order
        $lastOrder = self:: orderBy('created_at', 'desc')->first();

        if (!$lastOrder) {
            // We get here if there is no order at all
            // If there is no number set it to 0, which will be 1 at the end.
            $number = 0;
        } else {
            $number = explode('-', $lastOrder->tracking_number);

            $number = $number[1];
        }
        // If we have ORD000001 in the database then we only want the number
        // So the substr returns this 000001

        // Add the string in front and higher up the number.
        // the %06d part makes sure that there are always 6 numbers in the string.
        // so it adds the missing zero's when needed.

        return sprintf('%010d', (int)$number + 1);
    }


    public function productType()
    {
        return $this->belongsTo(ProductType::class, 'product_type', 'id');
    }

    public function createBy(){
        return $this->belongsTo(User::class,'created_by','id');
    }




}
