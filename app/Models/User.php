<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'status' => 'boolean'
    ];

    public function scopeActive(Builder $builder): Builder
    {
        return $builder->where('status', true);
    }

    public function scopeCustomer(Builder $builder): Builder
    {
        return $builder->where('role', 'customer');
    }

    public function scopeEmployee(Builder $builder): Builder
    {
        return $builder->where('role', 'employee');
    }

    public function scopeAdmin(Builder $builder): Builder
    {
        return $builder->where('role', 'admin');
    }
}
