<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OrderStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'sender_name' => 'required',
            'sender_email' => 'required|email',
            'sender_phone' => 'required',
            'sender_address' => 'required',
            'quantity' => 'required',
            'weight' => 'required',
            'receiver_name' => 'required',
            'receiver_email' => 'required',
            'receiver_phone' => 'required',
            'reciver_address' => 'required',
            'product_type' => 'required'
        ];
    }
}
