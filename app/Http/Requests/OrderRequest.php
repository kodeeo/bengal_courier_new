<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
            return  auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'sender_name' => 'required',
            'sender_phone' =>'required|min:8|regex:/[0-9]/',
            'sender_address' => 'required',
            'sender_email' => 'required|email',
            'description'=>'sometimes|required',
            'receiver' => 'required',
            'receiver_phone' => 'required|min:8|regex:/[0-9]/',
            'reciver_address' => 'required',
            'receiver_email' =>'required|email',
            'status' => 'sometimes|required',
            'product_type' => 'required',
            'weight' =>'required',
            'quantity' => 'required|integer',
        ];
    }

}
