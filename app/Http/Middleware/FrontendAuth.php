<?php

namespace App\Http\Middleware;

use Closure;

class FrontendAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!auth ()->check ()) {
            notify ()->warning ('Please login ');
            return redirect ()->route ('frontend.loginForm');
        }
        if(auth()->user()->role==='admin'){
            return redirect()->route('dashboard.index');
        }
        return $next($request);
    }
}
