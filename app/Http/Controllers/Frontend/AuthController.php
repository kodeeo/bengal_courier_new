<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Rules\MatchOldPassword;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class AuthController extends Controller
{
    public function singUpFormShow()
    {
        return view ('frontend.auth.registration');
    }


    public function showLoginForm()
    {
        return view ('frontend.auth.login');
    }


    public function registrationProcess(Request $request)
    {
        $this->validate ($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'phone' => 'required',
            'password' => 'required|confirmed',
            'account_type' => 'required'
        ]);

        $data = [
            'name' => $request->input ('name'),
            'email' => $request->email,
            'phone' => $request->phone,
            'password' => bcrypt ($request->input ('password')),
            'role' => 'customer',
            'status' => 1,
            'account_type' => $request->account_type
        ];

        try {
            User::create ($data);
            notify ()->success ('Your account successfully created.');
            return redirect ()->route ('frontend.loginForm');
        } catch (\Exception $exception) {
            notify ()->error ('Something went wrong!.Please try again');
            return redirect ()->back ();
        }
    }


    public function loginProcess(Request $request)
    {
        $this->validate ($request, [
            'login' => 'required',
            'password' => 'required',
        ]);

        $login_type = filter_var ($request->input ('login'), FILTER_VALIDATE_EMAIL)
            ? 'email'
            : 'username';
        $request->merge ([
            $login_type => $request->input ('login')
        ]);
        $credentials = $request->only ($login_type, 'password');

        $credentials['status'] = 1;
        if (Auth::attempt ($credentials)) {
            notify ()->success ('Successfully login.');
            return redirect ()->route ('frontend.dashboard');
        }

        notify ()->error ('Invalid Credentials.');
        return redirect ()->back ()->withInput ();
    }

    public function logout()
    {
        \auth ()->logout ();
        session ()->invalidate ();
        notify ()->success ('Successfully logout.');
        return redirect ()->route ('frontend.loginForm');
    }


    public function update(Request $request)
    {
        $this->validate ($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,'.\auth ()->user ()->id,
            'phone' => 'required',
            'account_type' => 'required'
        ]);

        $data = [
            'name' => $request->input ('name'),
            'email' => $request->email,
            'phone' => $request->phone,
            'role' => 'customer',
            'status' => 1,
            'account_type' => $request->account_type
        ];

        try {
            $user = User::find (\auth ()->user ()->getAuthIdentifier ());
            $user->update ($data);
            notify ()->success ('Your account successfully update.');
            return redirect ()->route ('frontend.dashboard');
        } catch (\Exception $exception) {
            notify ()->error ('Something went wrong!.Please try again');
            return redirect ()->back ();
        }
    }

    public function passwordChange(Request $request)
    {

        $request->validate ([
            'current_password' => ['required', new MatchOldPassword],
            'new_password' => ['required'],
            'new_confirm_password' => ['same:new_password'],
        ]);

        try {
            User::find (\auth ()->user ()->id)
                ->update ([
                    'password' => bcrypt ($request->input ('new_password'))
                ]);
            return $this->logout ();
        } catch (\Exception $exception) {
            notify ()->error ('Something went wrong!.Please try again');
            return redirect ()->back ()->with ('error');
        }

    }


}
