<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Requests\OrderStoreRequest;
use App\Models\Order;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function store(OrderStoreRequest $request)
    {
        $tracking_number = 'BGL-' . Order::getNextInvoiceNumber();

        $data = [
            'user_id' => auth()->user()->id,
            'sender_name' => $request->input('sender_name'),
            'sender_phone' => $request->input('sender_phone'),
            'sender_email' => $request->input('sender_email'),
            'sender_address' => $request->input('sender_address'),
            'receiver_name' => $request->input('receiver_name'),
            'receiver_phone' => $request->input('receiver_phone'),
            'receiver_address' => $request->input('receiver_email'),
            'receiver_email' => $request->input('receiver_email'),
            'tracking_number' => $tracking_number,
            'description' => $request->input('description'),
            'note' => $request->input('note'),
            'status' => Order::PENDING,
            'product_type' => $request->input('product_type'),
            'weight' => $request->input('weight'),
            'quantity' => $request->input('quantity'),
        ];

        try {
            Order::create($data);
            notify()->success('Your request has been seend.');
            return redirect()->back();
        } catch (\Exception $exception) {
            dd($exception->getMessage());
        }
    }
}
