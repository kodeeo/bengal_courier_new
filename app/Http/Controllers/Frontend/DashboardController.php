<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\ProductType;
use App\Models\ShippingType;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class DashboardController extends Controller
{
    public function index()
    {

        $data['total_recived']=Order::where('status',Order::RECEIEVED)->where('user_id',auth()->user()->id)->count();
        $data['total_pending']=Order::where('status',Order::PENDING)->where('user_id',auth()->user()->id)->count();
        $data['total_return']=Order::where('status',Order::RETURNT)->where('user_id',auth()->user()->id)->count();
        $data['total_deliverde']=Order::where('status',Order::DELIVERD)->where('user_id',auth()->user()->id)->count();
        $data['total_request']=Order::where('user_id',auth()->user()->id)->count();

        $data['product_types'] = ProductType::all (['id', 'name']);
        $data['shipping_types'] = ShippingType::all (['id', 'name']);
        $data['tracking_number'] = 'BLG-'.Order::getNextInvoiceNumber ();
        $data['recent_requests'] = Order::where ('user_id',
            auth ()->user ()->getAuthIdentifier ())->whereDay ('created_at', Carbon::today ())->get ();
        return view ('frontend.dashboard.index', $data);
    }

    public function shipping()
    {

        $data['product_types'] = ProductType::all (['id', 'name']);
        $data['shipping_types'] = ShippingType::all (['id', 'name']);
        $data['tracking_number'] = 'BGL-'.Order::getNextInvoiceNumber ();
        return view ('frontend.dashboard.addShipping', $data);
    }
}
