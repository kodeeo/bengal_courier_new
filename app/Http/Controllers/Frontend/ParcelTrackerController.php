<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Order;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class ParcelTrackerController extends Controller
{

    public function trackParcel()
    {
        return view('frontend.track.index');
    }


    public function track(Request $request)
    {
        $this->validate($request, [
            'tracker_number' => 'required'
        ]);

        $tracker_number = $request->input('tracker_number');
        try {
            if (auth()->check()) {
                $order = Order::where('user_id', auth()->user()->id)
                    ->where('tracking_number', $tracker_number)
                    ->first();
            } else {
                $order = Order::where('tracking_number', $tracker_number)
                    ->first();
              }

            if ($order !== null) {
                notify()->success('Data found');
                return view('frontend.track.trackHistory', compact('order'));
            }
            notify()->error('No found this number ' . $tracker_number);
            return redirect()->back()->with('error');
        } catch (ModelNotFoundException $exception) {
            notify()->warning('Something went wrong!.Please try again');
            return redirect()->back()->with('error');
        }


    }
}
