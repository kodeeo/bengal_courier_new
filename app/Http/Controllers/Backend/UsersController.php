<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\AdminRequest;
use App\Models\User;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    public function index()
    {
        $users = User::admin()->orderBy('id','desc')->paginate(10);
        return view('backend.users.index', compact('users'));
    }

    public function create()
    {
        return view('backend.users.create');
    }

    public function store(AdminRequest $request)
    {

        try {
            $data = [
                'name' => $request->input('name'),
                'email' => $request->email,
                'phone' => $request->phone,
                'password' => bcrypt($request->input('password')),
                'role' => $request->input('role'),
                'office' => $request->input('office'),
                'status' => $request->input('status') ? '1' : '0',
            ];
            $user = User::create($data);
            notify()->success('User created.');

            return redirect()->route('users.index');
        } catch (\Exception $exception) {
            notify()->error('Something went wrong.Please try again');
            return redirect()->back();
        }
    }


    public function login()
    {
        if(auth()->check() && auth()->user()->role==='admin'){
            return redirect()->route('dashboard.index');
        }
        return view('backend.Auth.login');
    }

    public function empolyeList(){
        $users = User::employee()->orderBy('id','desc')->paginate(10);
        return view('backend.users.employee.employee',compact('users'));
    }

    public  function  employeeCreate(){
        return view('backend.users.employee.create');
    }

    public function customerList(){
        $users = User::customer()->orderBy('id','desc')->paginate(10);
        return view('backend.users.customer.customer',compact('users'));
    }

    public  function  customerCreate(){
        return view('backend.users.customer.create');
    }

    public function delete($id){
            try{

                $users = User::findOrfail($id);
                if($users->role=='admin')
                {
                    notify()->error('Admin can not be deleted.');
                }else
                {
                    $users->delete();
                    notify('User successfully deleted.');
                }
                return redirect()->back();
            }catch (\Exception $exception){
                return redirect()->back();
            }
    }

    public function edit($id){
        $user=User::findOrfail($id);
        return view('backend.users.edit',compact('user'));
    }

    public function update(Request $request,$id){
        $data = [
            'name' => $request->input('name'),
            'email' => $request->email,
            'phone' => $request->phone,
            'role' => $request->input('role'),
            'office' => $request->input('office'),
            'status' => $request->input('status') ? '1' : '0',
        ];
        try{
            User::findOrfail($id)->update($data);
            notify()->success('User update');
            return redirect()->route('users.index');
        }catch (\Exception $exception){
            notify()->error('something is wrong');
            return redirect()->back();
        }

    }
}
