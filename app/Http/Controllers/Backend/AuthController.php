<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function loginProcess(Request $request)
    {
        $this->validate ($request, [
            'login' => 'required',
            'password' => 'required',
        ]);
        $login_type = filter_var ($request->input ('login'), FILTER_VALIDATE_EMAIL)
            ? 'email'
            : 'username';
        $request->merge ([
            $login_type => $request->input ('login')
        ]);
        $credentials = $request->only ($login_type, 'password');

        $credentials['status'] = 1;

        if (Auth::attempt ($credentials)) {
            notify ()->success ('Successfully login.');
            return redirect ()->route ('dashboard.index');
        }
        notify ()->error ('These credentials do not match our records.');

        return redirect ()->back ()->withInput ();
    }

    public function logout()
    {
        \auth ()->logout ();
        session()->invalidate();
        notify ()->success ('Successfully logout.');
        return redirect ()->route ('users.login');
    }
}
