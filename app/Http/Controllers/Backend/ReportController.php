<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class ReportController extends Controller
{
    public function index()
    {
        $orders = Order::all();
        $total = $orders->sum('deliver_charge');
        return view('backend.report.parcel_report', compact('orders', 'total'));
    }

    public function generateReport(Request $request)
    {
        $form_date = Carbon::parse($request->form_date)
            ->format('Y-m-d');
        $to_date = Carbon::parse($request->to_date)
            ->format('Y-m-d');

        if ($request->input('form_date') != null && $request->input('to_date') != null) {
            $orders = Order::where('status', $request->status)
                ->whereBetween('created_at', [$form_date, $to_date])
                ->get();
        } else {
            $orders = Order::where('status', $request->status)->get();
        }
        $total = $orders->sum('deliver_charge');
        return view('backend.report.parcel_report', compact('orders', 'total'));
    }
}
