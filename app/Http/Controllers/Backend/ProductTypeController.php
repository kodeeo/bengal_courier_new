<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\ProductType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProductTypeController extends Controller
{
    public function index()
    {
        $product_types = ProductType::paginate(10);

        return view('backend.productsType.index', ['product_types' => $product_types]);
    }

    public function create()
    {
        return view('backend.productsType.create');
    }


    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'type_of_packing' => 'required',
            'dimension' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        try {
            $data = [
                'name' => $request->name,
                'type_of_packing' => $request->type_of_packing,
                'dimension' => $request->dimension,
                'status' => $request->status ? '1' : '0'
            ];
            ProductType::create($data);
            notify()->success('Product Type created successfully.');

            return redirect()->route('productTypes.index');
        } catch (\Exception $exception) {
            notify()->error('Something went wrong.Please try again.');
        }
    }
    public function edit($id){
        $product_type=ProductType::findOrfail($id);
        return view('backend.productsType.edit',compact('product_type'));
    }
    public function update(Request $request,$id){
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'type_of_packing' => 'required',
            'dimension' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $data = [
            'name' => $request->name,
            'type_of_packing' => $request->type_of_packing,
            'dimension' => $request->dimension,
            'status' => $request->status ? '1' : '0'
        ];
      try{
          ProductType::findOrFail($id)->update($data);
          notify()->success('Product type sucessfully update');
          return redirect()->route('productTypes.index');
      }catch (\Exception $exception){
          notify()->error('Something is wrong');
          return redirect()->back();
      }
    }
}
