<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Order;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {

        $pending_order = Order::where('status', Order::PENDING)->count();
        $delivered_order = Order::where('status', Order::DELIVERD)->count();
        $recived_order = Order::where('status', Order::RECEIEVED)->count();
        $return_order = Order::where('status', Order::RETURNT)->count();

        return view('backend.index', compact(
            'pending_order',
            'delivered_order',
            'return_order',
            'recived_order'
        ));
    }
}
