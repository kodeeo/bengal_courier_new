<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\OrderRequest;
use App\Models\Order;
use App\Models\ProductType;
use App\Models\ShippingType;
use Barryvdh\DomPDF\Facade as PDF;
use Dompdf\Dompdf;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\View\View;
use Milon\Barcode\DNS1D;
use Milon\Barcode\DNS2D;

class OrderController extends Controller
{
    public function index()
    {
        $orders = Order::paginate(10);
        return view('backend.orders.index', compact('orders'));
    }


    public function create()
    {
        $data['product_types'] = ProductType::all(['id', 'name']);
        $data['shipping_types'] = ShippingType::all(['id', 'name']);

        return view('backend.orders.create', $data);
    }

    public function store(OrderRequest $request)
    {
        $tracking_number = 'BGL-' . Order::getNextInvoiceNumber();

        $data = [
            'user_id' => auth()->user()->getAuthIdentifier(),
            'sender_name' => $request->input('sender_name'),
            'sender_email'=>$request->input('sender_email'),
            'sender_phone' => $request->input('sender_phone'),
            'sender_address' => $request->input('sender_address'),
            'description'=>$request->input('details'),
            'receiver_name' => $request->input('receiver'),
            'receiver_phone' => $request->input('receiver_phone'),
            'receiver_address' => $request->input('receiver_email'),
            'receiver_email' => $request->input('receiver_email'),
            'tracking_number' => $tracking_number,
            'status' => $request->input('status'),
            'product_type' => $request->input('product_type'),
            'collection_date_time' => $request->input('collection_date'),
            'delivery_date_time' => $request->input('delivery_date_time'),
            'payment_status' => $request->input('payment_status'),
            'weight' => $request->input('weight'),
            'quantity' => $request->input('quantity'),
        ];

        try {
            Order::create($data);
            notify()->success('Order successfully created.');
            return redirect()->route('orders.index');
        } catch (\Exception $exception) {
            notify()->error('Something went wrong.');
            return redirect()->back();
        }
    }


    public function onlineOrderList()
    {
        $orders = Order::paginate(10);
        return view('backend.orders.onlineOrdersList', compact('orders'));
    }

    public function show($id)
    {
        $order = Order::findOrfail($id);
        return view('backend.orders.show', compact('order'));
    }

    public function update(Request $request, $id)
    {
        $data = [
            'status' => $request->input('status'),
            'payment_status' => $request->input('payment_status'),
            'deliver_charge' => $request->input('deliver_charge'),
            'collection_date_time'=>date($request->input('collect_date')),
            'delivery_date_time'=>date($request->input('deliver_date'))
        ];
        try {
            $order = Order::find($id);
            $order->update($data);
            notify()->success('Sucessfully update');
            return redirect()->back();
        } catch (\Exception $exception) {
           notify()->error('Something is wrong');
            return redirect()->back();
        }
    }

    public function printInvoice($id)
    {
        $order = Order::find($id);
        $pdf = PDF::loadView('backend.orders.invoice', compact('order'))->setPaper('a4', 'portrait');
        return $pdf->stream('invoice-'.$order->id);
    }


}
