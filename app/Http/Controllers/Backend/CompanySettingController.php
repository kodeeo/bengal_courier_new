<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Setting;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class CompanySettingController extends Controller
{
    public function show()
    {
        $setting=Setting::first();
        return view('backend.company-setting.edit',compact('setting'));
    }


    public function update(Request $request,Setting $setting)
    {

        $this->validate($request,[
            'company_name'=>'required',
            'company_phone'=>'required|min:8|regex:/[0-9]/',
            'email'=>'required|email',
            'country'=>'required',
            'city'=>'required',
            'address'=>'required',
        ]);

            try{
                if($request->hasFile('logo')){
                   $file=$request->file('logo');
                   $random_file_name=Str::random(20).'.'.$file->getClientOriginalExtension();
                   $file->move('public/upload/logo',$random_file_name);
                $setting->update(
                    [
                        'company_name'=>$request->input('company_name'),
                        'company_phone'=>$request->input('company_phone'),
                        'email'=>$request->input('email'),
                        'country'=>$request->input('country'),
                        'city'=>$request->input('city'),
                        'address'=>$request->input('address'),
                        'logo'=>$random_file_name
                    ]
                );
                }else{
                    $setting->update(
                        [
                            'company_name'=>$request->input('company_name'),
                            'company_phone'=>$request->input('company_phone'),
                            'email'=>$request->input('email'),
                            'country'=>$request->input('country'),
                            'city'=>$request->input('city'),
                            'address'=>$request->input('address'),
                        ]
                    );
                }
                notify()->success('Setting update');
                return redirect()->back();
            } catch (ModelNotFoundException $exception){
                notify()->error('Something is went.');
               return redirect()->back();
            }

    }

}
