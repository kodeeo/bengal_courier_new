@extends('frontend.Layouts.app')
@section('main')
    <div class="container mt-4 mb-3">
        <div class="row">
            <div class="col-md-6">
                <div class="card border-0 rounded-lg bg-white p-4 shadow-sm">
                    <form action="{{route ('frontend.login')}}" method="post">
                        @csrf
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-12 col-form-label">Email *</label>
                            <div class="col-sm-12">
                                <input required type="email" value="{{old ('login')}}" name="login"
                                       class="form-control rounded-lg @error('login') is-invalid @enderror"
                                       id="inputEmail3">
                                @error('login') <span class="text-danger">{{$message}}</span> @enderror
                            </div>

                        </div>
                        <div class="form-group row mt-2">
                            <label for="inputPassword3" class="col-sm-12 col-form-label">Password *</label>
                            <div class="col-sm-12">
                                <input required type="password" name="password"
                                       class="form-control rounded-lg @error('password') is-invalid @enderror"
                                       id="inputPassword3">
                                @error('password') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-12">
                                <a href="#" class="text-dark font-weight-bold m-5">Forgot Password ?</a>
                                <button type="submit" class="btn btn-primary px-5 rounded-lg font-weight-bold">Sign in</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card border-0 rounded-lg bg-white shadow-sm p-4">
                    <div>
                        <h4 class="text-black-50 bold mb-4">Sign up with us and enjoy some great benefits.</h4>

                        <p class="">  Plenty of choice with dozens of services.</p>
                        <p >Delivery updates and offers, especially for you.</p>
                        <p> Extra benefits + including cash-back for future bookings.</p>
                        <a  class="btn btn-success" href="{{route ('sing-up')}}">Sing up</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
