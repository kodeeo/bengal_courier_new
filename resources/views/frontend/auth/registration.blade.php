@extends('frontend.Layouts.app')


@section('main')
    <div class="container mt-4 mb-3">
        <div class="row">
            <div class="col-md-12">
                <div class="card border-0 shadow-sm rounded-lg">
                    <div class="card-header bg-white">
                        <h5> Sing up form</h5>
                    </div>
                    <div class="card-body">
                        <form method="post" action="{{route ('frontend.registration')}}">
                            @csrf
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="card px-2 py-2 border-info rounded-lg">
                                        <div class="form-row">
                                            <div class="form-group col-md-12">
                                                <label for="inputEmail4">Name:<span
                                                            class="text-danger">*</span></label>
                                                <input type="text" name="name"
                                                       class="form-control rounded-lg @error('name') is-invalid @enderror"
                                                       id="inputEmail4" value="{{old ('name')}}">

                                                @error('name') <span class="text-danger">{{$message}}</span> @enderror
                                            </div>
                                            <div class="form-group col-md-12">
                                                <label for="inputEmail4">Email<span class="text-danger">*</span></label>
                                                <input type="email" name="email"
                                                       class="form-control rounded-lg  @error('email') is-invalid @enderror"
                                                       id="inputEmail4" value="{{old ('email')}}">
                                                @error('email') <span class="text-danger">{{$message}}</span> @enderror
                                            </div>
                                            <div class="form-group col-md-12">
                                                <label for="inputEmail4">Mobile<span
                                                            class="text-danger">*</span></label>
                                                <input type="text" name="phone"
                                                       class="form-control rounded-lg  @error('phone') is-invalid @enderror"
                                                       id="inputEmail4" value="{{old ('phone')}}">
                                                @error('phone') <span class="text-danger">{{$message}}</span> @enderror
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="form-group col-md-12">
                                                <div class="custom-control custom-radio custom-control-inline">
                                                    <input type="radio" id="user_type"
                                                           name="account_type" value="customer"
                                                           class="custom-control-input">
                                                    <label class="custom-control-label" for="user_type">Customer</label>
                                                </div>
                                                <div class="custom-control custom-radio custom-control-inline">
                                                    <input type="radio" id="user_type1"
                                                           name="account_type" value="merchant"
                                                           class="custom-control-input">
                                                    <label class="custom-control-label"
                                                           for="user_type1">Merchant</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-md-6">
                                    <div class="card p-3 border border-info rounded-lg">
                                        <div class="form-row">
                                            <div class="form-group col-md-6">
                                                <label for="inputEmail4">
                                                    Password
                                                    <span class="text-danger">*</span>
                                                </label>
                                                <input type="password" name="password" value="{{old ('password')}}"
                                                       class="form-control rounded-lg @error('password') is-invalid @enderror"
                                                       id="inputEmail4">
                                                @error('password') <span
                                                        class="text-danger">{{$message}}</span> @enderror
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="inputEmail4">
                                                    Confirm password
                                                    <span class="text-danger">*</span>
                                                </label>
                                                <input type="password" name="password_confirmation"
                                                       class="form-control rounded-lg @error('password_confirmation') is-invalid @enderror"
                                                       id="inputEmail4">
                                                @error('password_confirmation') <span
                                                        class="text-danger">{{$message}}</span> @enderror
                                            </div>
                                        </div>
                                        <div class="d-flex justify-content-center mt-4">
                                            <button type="submit" class="btn btn-primary rounded-lg px-5">Sign up
                                            </button>
                                        </div>
                                        <div class="d-flex justify-content-center mt-4">
                                            <p class="font-weight-bold font-weight-lighter">Do you have an account
                                                please <br/>
                                                Click On <a href="{{route ('frontend.loginForm')}}">Sing In</a>
                                            </p>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection