<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="icon"
          type="image/png"
          href="{{asset('/images/logo.png')}}">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.1/css/all.min.css">
    <link rel="stylesheet" type="text/css" href="{{asset ('/')}}css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="{{asset ('/')}}css/all.min.css">

    <!-- 3.owl carousel CSS -->
    <link rel="stylesheet" type="text/css" href="{{asset ('/')}}css/owl.carousel.min.css">

    <link rel="stylesheet" href="{{asset ('css/app.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset ('/')}}css/style.css">
    @notifyCss
    <link rel="stylesheet" href="{{asset ('css/dashboard.css')}}">
    <title>@yield('title',config('app.name'))</title>

</head>


<body>

<!-- navbar start -->

@includeIf('frontend.partials.header')


<!-- navbar end -->

@yield('main')



<!-- footer section start -->
@include('frontend.partials.footer')



<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

@include('notify::messages')
@notifyJs
@stack('js')
</body>
</html>
