<div class="card rounded-lg shadow-sm border-0 overflow-auto">
    <div class="card-header bg-white">
        <div class="spur-card-icon">
            <i class="fas fa-chart-bar"></i>
        </div>
        <div class="spur-card-title">Shipping</div>
    </div>
    <div class="card-body border">
        <form action="{{route('frontend.request')}}" method="post">
            @csrf
            <div class="row">
                <div class="col-lg-4 col-md-4">
                    <div class="card border-0 rounded-lg shadow-sm">
                        <div class="card-header"><span class="font-weight-bold"> Sender Information</span></div>
                        <div class="card-body">
                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <input required type="text" class="form-control @error('sender_name') is-invalid @enderror rounded-lg" name="sender_name" id="sender"
                                           placeholder="Full name" value="{{old('sender_name',auth ()->user ()->name)}}">
                                    @error('sender_name')  <span class="text-danger">{{$message}}</span>@enderror
                                </div>
                                <div class="form-group col-md-12">
                                    <input required type="text" class="form-control @error('sender_email') is-invalid @enderror rounded-lg" name="sender_email" id="sender"
                                           placeholder="Full name" value="{{old('sender_email',auth ()->user ()->email)}}">
                                    @error('sender_email')  <span class="text-danger">{{$message}}</span>@enderror

                                </div>
                                <div class="form-group col-md-12">
                                    <input type="text" class="form-control rounded-lg" id="phone"
                                           placeholder="Mobile No" value="{{auth ()->user ()->phone}}"
                                           name="sender_phone">
                                   @error('sender_phone')  <span class="text-danger">{{$message}}</span>@enderror
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <textarea type="text" class="form-control rounded-lg" id="sender"
                                              placeholder="address"
                                              name="sender_address">{{auth ()->user ()->address}}</textarea>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <input type="number" class="form-control rounded-lg @error('quantity') is-invalid @enderror" id="sender"
                                           placeholder="quantity" name="quantity" value="{{old('quantity')}}">
                                </div>
                                <div class="form-group col-md-6">
                                    <input required type="number" class="form-control rounded-lg @error('weight') is-invalid @enderror" id="sender"
                                           placeholder="weight(kg)" name="weight" value="{{old('weight')}}">
                                    @error('weight') <span class="text-danger">{{$message}}</span> @enderror
                                </div>

                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <textarea  class="form-control @error('description') is-invalid @enderror rounded-lg" id="sender" name="description"
                                              placeholder="Product details">{{old('description')}}</textarea>
                                    @error('description') <span class="text-danger">{{$message}}</span> @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5 col-md-5">
                    <div class="card rounded-lg border-0 shadow-sm">
                        <div class="card-header">
                            <span class="font-weight-bold">Receiver Information</span>
                        </div>
                        <div class="card-body">
                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <input name="receiver_name" required type="text" class="form-control rounded-lg" id="sender"
                                           placeholder="Full name" value="{{old('receiver_name')}}">
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <input type="text" class="form-control rounded-lg" id="phone"
                                           placeholder="Receiver Email" required name="receiver_email" value="{{old('receiver_email')}}">
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <input name="receiver_phone" type="text" class="form-control rounded-lg" id="phone"
                                           placeholder="Mobile No" required>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label for="sender">Address</label>
                                    <textarea type="text" required class="form-control rounded-lg" id="sender"
                                              placeholder="Receiver address"
                                              name="reciver_address">{{auth ()->user ()->address}}</textarea>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <textarea required type="text" class="form-control rounded-lg" id="phone" placeholder="Note"
                                              name="note">{{old('note')}}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3">
                    <div class="card rounded-lg border-0 shadow-sm">
                        <div class="card-header">
                            <span class="font-weight-bold space" style="color:#1a202c"> Product Type</span>
                        </div>

                        <div class="card-body">
                            @foreach($product_types as $key=>$product_type)
                                <div class="custom-control custom-radio">
                                    <input type="radio" name="product_type" required id="customRadio{{$key}}"
                                           class="custom-control-input" value="{{$product_type->id}}">
                                    <label class="custom-control-label" for="customRadio{{$key}}">
                                        {{$product_type->name}}
                                    </label>
                                </div>
                                @error('product_type') {{$message}} @enderror
                            @endforeach
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-info rounded-lg float-right text-uppercase"><i
                                    class="fa fa-save mr-2"></i>Send Request
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
