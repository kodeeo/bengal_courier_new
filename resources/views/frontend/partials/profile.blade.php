<div class="container">
    <div class="row">
        <div class="col-lg-6">
            <form action="{{route ('profile.update')}}" method="post">
                @csrf
                <div class="card border-0 rounded-lg p-5">
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="sender">Name</label>
                            <input type="text" class="form-control rounded-lg"
                                   name="name" id="name"
                                   placeholder="Name"
                                   value="{{auth ()->user ()->name}}">
                        </div>
                        <div class="form-group col-md-12">
                            <label for="sender">Email</label>
                            <input type="text" class="form-control rounded-lg"
                                   name="email" id="sender"
                                   placeholder="Sender Name"
                                   value="{{auth ()->user ()->email}}">
                        </div>
                        <div class="form-group col-md-12">
                            <label for="sender">Mobile no</label>
                            <input type="text" class="form-control rounded-lg"
                                   name="phone" id="sender"
                                   placeholder="Sender Name"
                                   value="{{auth ()->user ()->phone}}">
                        </div>
                        @if(auth ()->user ()->account_type === 'customer')
                            <div class="form-row col-md-12 mb-3">
                                <div
                                    class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="user_type"
                                           name="account_type" value="customer"
                                           class="custom-control-input" checked>
                                    <label class="custom-control-label"
                                           for="user_type">Customer</label>
                                </div>
                            </div>
                        @else
                            <div
                                class="custom-control mb-3 custom-radio custom-control-inline">
                                <input type="radio" id="user_type1"
                                       name="account_type" value="merchant"
                                       class="custom-control-input" checked>
                                <label class="custom-control-label"
                                       for="user_type1">Merchant</label>
                            </div>
                        @endif
                    </div>
                    <div class="form-row">
                        <button type="submit" class="btn btn-info rounded-lg shadow-sm">
                            Update Profile
                        </button>
                    </div>

                </div>
            </form>
        </div>
        <div class="col-lg-6">
            <form action="{{route ('passwordChange')}}" method="post">
                @csrf
                <div class="card border-0 rounded-lg p-5">
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="sender">Old Password</label>
                            <input type="password" class="form-control rounded-lg"
                                   name="current_password" id="sender"
                                   placeholder="************">
                        </div>

                        <div class="form-group col-md-12">
                            <label for="sender">New Password</label>
                            <input type="password" class="form-control rounded-lg"
                                   name="new_password" id="sender"
                                   placeholder="************">
                        </div>
                        <div class="form-group col-md-12">
                            <label for="sender">Confirm Password</label>
                            <input type="password" class="form-control rounded-lg"
                                   name="new_confirm_password" id="sender"
                                   placeholder="************">
                        </div>
                    </div>
                    <div class="form-row">
                        <button type="submit" class="btn btn-info rounded-lg shadow-sm">
                            Change password
                        </button>
                    </div>
                </div>
            </form>
        </div>

    </div>
</div>
