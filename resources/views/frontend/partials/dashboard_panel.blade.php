<div class="container">
    <div class="row">
        <div class="col-lg-3 pb-4  ">
            <div
                class="card bg-info d-flex justify-content-center align-middle shadow-sm border-0 rounded-lg"
                style="height: 100px;">
                <p class="text-center text-white font-weight-bold text-light">Total Request
                    <span>{{$total_request}}</span></p>
            </div>
        </div>

        <div class="col-lg-3 pb-4 ">
            <div
                class="card bg-primary d-flex justify-content-center align-middle shadow-sm border-0 rounded-lg"
                style="height: 100px;">
                <p class="text-center">Total Deliverde <span>{{$total_deliverde}}</span></p>
            </div>
        </div>
        <div class="col-lg-3 pb-4 ">
            <div
                class="card  bg-warning d-flex justify-content-center align-middle shadow-sm border-0 rounded-lg"
                style="height: 100px;">
                <p class="text-center">Pending Request <span>{{$total_pending}}</span></p>
            </div>
        </div>
        <div class="col-lg-3 pb-4 ">
            <div
                class="card d-flex justify-content-center align-middle shadow-sm border-0 rounded-lg"
                style="height: 100px;">
                <p class="text-center">Total Received <span>{{$total_recived}}</span></p>
            </div>
        </div>
        <div class="col-lg-3 pb-4 ">
            <div
                class="card d-flex justify-content-center align-middle shadow-sm border-0 rounded-lg"
                style="height: 100px;">
                <p class="text-center">Total Return <span>{{$total_return}}</span></p>
            </div>
        </div>
    </div>
</div>
