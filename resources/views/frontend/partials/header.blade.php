<section class="navbar-section border-bottom border-primary">
    <div class="container">
        <div class="full-width">
            <div class="row">
                <div class="col-xl-2">
                    <div class="logo">
                        <a href="{{route ('home')}}">
                            <img src="{{asset('/images/logo.png')}}" alt="logo">
                        </a>
                    </div>
                </div>
                <div class="col-xl-7 line-highlight pt-2">
                    <ul class="menu">
                        @auth
                            <li><a href="{{route ('parcel.track')}}">Track My Parcel</a></li>
                            <li><a href="">About Us</a></li>
                            <li><a href="">Contact US</a></li>
                        @endauth
                        @guest
                            <li><a href="">Get a Quote</a></li>
                            <li><a href="{{route ('parcel.track')}}">Track My Parcel</a></li>
                            <li><a href="{{route ('sing-up')}}">SIGN UP</a></li>
                            <li><a href="">About Us</a></li>
                            <li><a href="">Contact US</a></li>
                        @endguest

                    </ul>
                </div>
                <div class=" col-xl-3 log-section pt-2 ">
                    @auth

                        <a class="log-in d-inline-block mr-3" href="{{route ('frontend.logout')}}">
                            <i class="fas fa-lock"></i>
                            Logout
                        </a>
                        <a class="log-in text-primary d-inline-block ml-4" href="{{route ('frontend.dashboard')}}">
                            <i class="fas fa-user text-primary"></i>
                            My Account
                        </a>

                    @endauth
                    @guest
                        <a class="log-in text-success" href="{{route ('frontend.loginForm')}}">
                            <i class="fas fa-user"></i>
                            Log In
                        </a>
                    @endguest

                </div>

            </div>
        </div>
    </div>
</section>
