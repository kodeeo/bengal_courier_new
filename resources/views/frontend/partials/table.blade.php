<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="card border-0 rounded-lg py-4">
                <table class="table">
                    <thead>
                    <tr class="border-top-0">
                        <th scope="col">#</th>
                        <th scope="col">tracking_number</th>
                        <th scope="col">Product</th>
                        <th scope="col">status</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($recent_requests as $key=>$recent_request)
                        <tr>
                            <th scope="row">{{$key+1}}</th>
                            <td>{{$recent_request->tracking_number}}</td>
                            <td>{{optional($recent_request->productType)->name}}</td>
                            <td>
                                                            <span
                                                                class="text-white py-2 rounded-lg badge badge-success">{{$recent_request->status}}</span>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>

    </div>
</div>
