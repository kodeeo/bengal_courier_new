<section class="footer-section">
    <div class="container">
        <div class="row">
            <div class="col-xl-3">
                <div class="footer-block">
                    <img src="{{asset('/images/logo.png')}}">
                </div>

            </div>

            <div class="col-xl-3">
                <div class="footer-block">
                    <p>WHO ARE WE?</p>
                    <ul>
                        <li><a href="About Us">About Us</a></li>
                        <li><a href="About Us">Contact Us</a></li>
                        <li><a href="About Us">Awards</a></li>
                    </ul>
                </div>
            </div>

            <div class="col-xl-3">
                <div class="footer-block">
                    <p>Tools</p>
                    <ul>
                        <li><a href="{{route ('frontend.dashboard')}}">My Accout</a></li>
                        <li><a href="About Us">Track a Parcel</a></li>
                        <li><a href="About Us">Get a Quote</a></li>
                    </ul>
                </div>
            </div>

            <div class="col-xl-3">
                <div class="footer-block">
                    <p>Features</p>
                    <ul>
                        <li><a href="About Us">Business Account</a></li>

                    </ul>
                </div>
            </div>
        </div>
        <div class="footer-media">
            <ul>
                <li class="fab"><a href=""><i class="fab fa-facebook-f"></i></a></li>
                <li class="twit"><a href=""><i class="fab fa-twitter"></i></a></li>
                <li class="link"><a href=""><i class="fab fa-linkedin-in"></i></a></li>
                <li class="google"><a href=""><i class="fab fa-google-plus-g"></i></a></li>
            </ul>

        </div>

    </div>
</section>
<!-- footer section end -->
<section class="footer-baseline">
    <div class="container">
        <div class="foot-content">
            <p>© Copyright 2016 DEPRIXA 2.5</p>
            <ul>
                <li><a href="">Private Policy</a></li>
                <li><a href="">Cookie Policy</a></li>
                <li><a href="">Terms and conditions</a></li>
                <li><a href="">Refund Policy</a></li>
            </ul>
        </div>
    </div>
</section>
