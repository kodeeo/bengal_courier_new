@extends('frontend.Layouts.app')


@section('main')
    <div class="container">
        <div class="shadow my-4 px-4 rounded-lg py-4 bg-white">
            <div class="row">
                <div class="col-lg-12 justify-content-center">
                    <div class="d-flex text-center justify-content-center align-items-center">
                        <p>
                        <span>
                            <svg width="23" height="25" viewBox="0 0 23 25" fill="#FF4F19"
                                 xmlns="http://www.w3.org/2000/svg">
                                <g clip-path="url(#clip0)">
                                <path
                                    d="M22.2009 21.2497L17.9165 16.5767C17.7232 16.3658 17.4611 16.2486 17.1861 16.2486H16.4857C17.6716 14.5939 18.3763 12.5131 18.3763 10.2491C18.3763 4.86359 14.3755 0.5 9.43836 0.5C4.50125 0.5 0.5 4.86359 0.5 10.2491C0.5 15.6345 4.50082 19.9981 9.43836 19.9981C11.5138 19.9981 13.422 19.2294 14.9388 17.9356V18.6997C14.9388 18.9997 15.0462 19.2856 15.2396 19.4966L19.524 24.1695C19.9279 24.6102 20.581 24.6102 20.9806 24.1695L22.1966 22.843C22.6005 22.4023 22.6005 21.6903 22.2009 21.2497V21.2497ZM9.43836 16.9981C6.02063 16.9981 3.25086 13.9822 3.25086 10.2491C3.25086 6.52109 6.0159 3.5 9.43836 3.5C12.8561 3.5 15.6259 6.51594 15.6259 10.2491C15.6259 13.977 12.8608 16.9981 9.43836 16.9981ZM9.43922 5.75C7.68695 5.75 6.26641 7.29922 6.26641 9.21078C6.26641 10.7558 8.34008 13.5725 9.13457 14.5948C9.17183 14.6435 9.21843 14.6826 9.27107 14.7095C9.32371 14.7363 9.38111 14.7502 9.43922 14.7502C9.49733 14.7502 9.55473 14.7363 9.60737 14.7095C9.66001 14.6826 9.7066 14.6435 9.74387 14.5948C10.5384 13.5725 12.612 10.7563 12.612 9.21078C12.612 7.29922 11.1915 5.75 9.43922 5.75V5.75ZM9.43922 10.25C8.86945 10.25 8.40797 9.74609 8.40797 9.125C8.40797 8.50344 8.86988 8 9.43922 8C10.0086 8 10.4705 8.50344 10.4705 9.125C10.4705 9.74609 10.0086 10.25 9.43922 10.25Z"
                                    fill="black"/>
                                </g>
                                <defs>
                                <clipPath id="clip0">
                                <rect x="0.5" y="0.5" width="22" height="24" fill="#FF4F19"/>
                                </clipPath>
                                </defs>
                                </svg>
                        </span>
                            <span class="text-dark text-lg">
                            parcel tracking</span>
                        </p>

                    </div>
                </div>
            </div>

            <div class="row border-bottom pb-3 align-items-center">
                <div class="col-lg-6"><p class="font-weight-bold" style="color:#FF4F19">{{$order->tracking_number}}</p></div>
                <div class="col-lg-6 d-flex">
                    <div>Current Status
                        <button class="btn btn-secondary rounded-lg">
                            {{$order->status}}
                        </button>
                    </div>
                    <div class="ml-5">
                        Booking Mode
                       @if($order->payment_status!==null)
                        <button class="btn btn-info rounded-lg">
                            {{$order->payment_status}}
                        </button>
                         @else
                            <button class="btn btn-danger rounded-lg">
                                Not yet payment
                            </button>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row py-4">
                <div class="col-lg-12">
                    <p class="font-weight-bold">Schedule Delivery</p>
                    @if($order->delivery_date_time!==null)
                    <p> {{$order->delivery_date_time->format('M j, Y \a\t g:i a')}}</p>
                     @endif
                </div>
            </div>
            <div class="row" >
                <div class="col-lg-4">
                    <span class="text-dark h4 d-block pb-3">Additional Information</span>
                    <p><span class="text-dark font-weight-bold mr-1">Origin</span>:Matijil</p>
                    <p><span class="text-dark font-weight-bold mr-1">Destination</span>:Bangladesh</p>
                    <p><span class="text-dark font-weight-bold mr-1">Weight</span>:{{$order->weight}} (kg)</p>
                    <p><span class="text-dark font-weight-bold mr-1">Pick update time</span>:{{$order->delivery_date_time}}</p>
                    <p><span class="text-dark font-weight-bold mr-1">Description</span>:{{$order->description}}</p>
                </div>
                <div class="col-lg-4">
                   <p class="font-weight-bold"> Shipper info</p>
                    <p><span class="text-dark font-weight-bold mr-1">Name</span>:{{$order->sender_name}}</p>
                    <p><span class="text-dark font-weight-bold mr-1">Phoen</span>:{{$order->sender_phone}}</p>
                    <p><span class="text-dark font-weight-bold mr-1">Address</span>:{{$order->sender_address}}</p>
                </div>
                <div class="col-lg-4">
                    <p class="font-weight-bold">Consignee</p>
                    <p><span class="text-dark font-weight-bold mr-1">Name</span>:{{$order->receiver_name}}</p>
                    <p><span class="text-dark font-weight-bold mr-1">Phone</span>:{{$order->receiver_phone}}</p>
                    <p><span class="text-dark font-weight-bold mr-1">Address</span>:{{$order->receiver_address}}</p>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card border-0 rounded-lg py-4">
                        <table class="table">
                            <thead>
                            <tr class="border-top-0">
                                <th scope="col">#</th>
                                <th scope="col">tracking_number</th>
                                <th scope="col">Product Type</th>
                                <th scope="col">status</th>
                            </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th scope="row">1</th>
                                    <td>{{$order->tracking_number}}</td>
                                    <td>{{$order->productType->name}}</td>
                                    <td>
                                        <span class="text-white py-2 rounded-lg badge badge-success">
                                            {{$order->status}}
                                        </span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
