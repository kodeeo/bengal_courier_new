@extends('frontend.Layouts.app')

@section('title') Track parcel @endsection

@section('main')
    <div>
        <div class="bg-white py-4">
            <div class="container">
                <h3 class="text-uppercase text-center">Is It Nearly there yet</h3>
                <p>
                    The pick-up and delivery is easy with us  and doing so could not simpler .Once Your delivery
you have booked through us . a tracking number is assigned example somethink like <strong>
                        BLG-0000000002
                    </strong>
                    just input this unique number into the box below ta-dah,you can find exactly where it is ?
                </p>
            </div>
        </div>
        <div style="background: #F16124;">
            <div class="container">
                <div class="justify-content-center py-3 row shadow-sm">
                    <div class="col-lg-5 text-white text-uppercase mt-2 italic">Enter your booking reference</div>
                    <div class="d-flex col-lg-7">
                        <form action="{{route ('parcel.find')}}" method="get">
                            @csrf
                            <div class="d-flex">
                                <div>
                                    <label>
                                        <input name="tracker_number" style="width: 300px" type="text"
                                               placeholder="BLG-0505000001"
                                               class="w-200 bg-gray border-0 py-2 pl-5  rounded-lg @error('tracker_number') is-invalid @enderror">
                                    </label>
                                </div>
                                <div>
                                    <button type="submit" class="btn btn-info ml-5 rounded-lg"><i class="fa fa-car"></i>
                                        Track My Parcel
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
        <div class="container mt-5 mb-5">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="card border-0 rounded-lg">
                        <div class="card-header">
                            <span class="text-dark font-weight-bold">PARCEL NOT YET COllECTED?</span>
                        </div>
                        <div class="card-body">
                            <p><span><svg width="30" height="30" fill="none" xmlns="http://www.w3.org/2000/svg"><path
                                            fill-rule="evenodd" clip-rule="evenodd"
                                            d="M12 23C5.925 23 1 18.075 1 12S5.925 1 12 1s11 4.925 11 11-4.925 11-11 11zm0-2a9 9 0 100-18 9 9 0 000 18zm-3.293-4.293L12 13.414l3.293 3.293 1.414-1.414L13.414 12l3.293-3.293-1.414-1.414L12 10.586 8.707 7.293 7.293 8.707 10.586 12l-3.293 3.293 1.414 1.414z"
                                            fill="red"/></svg>
                                </span>
                                <span class="line-highlight font-weight-lighter">
                                   Sorry ,this dosen't happen,We're sure there's good reason.
                                    if It's after 6pm on the booked collection day,
                                    reschedule collection here.
                                </span>
                            </p>
                            <a href="#">Purchaceh collection</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="card rounded-lg border-0">
                        <div class="card-header font-weight-bold">
                            NEED FAST A ANSWERER
                        </div>
                        <div class="card-body">
                            <p>
                                you've any question at all about your delivery,then don't hesitate to get in touch
                                with our uk based customer service team.But first check out oure FAQ's page as the
                                anwser you're looking for probably be there.
                            </p>
                            <a href="#">Contact us</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
