@extends('frontend.Layouts.app')


@section('main')
    <!-- slider start -->
    @includeIf('frontend.partials.slider')
    <!-- slider end -->


    <!-- shipping start -->
    <section class="shipping-section">
        <div class="container">
            <div class="row">
                <div class="col-xl-6">
                    <div class="single-ship">
                        <p><i class="far fa-times-circle"></i>Free Shipping</p>
                        <img src="{{asset('/images/freeshipping.png')}}">
                    </div>
                </div>
                <div class="col-xl-6">
                    <div class="single-ship">
                        <p><i class="fas fa-history"></i>Free Shipping</p>
                        <img src="{{asset('images/dropshipping (2).png')}}">
                        <a href="">Contact Us</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- shipping end -->

    <!-- compare section start -->

    <section class="compare-section">
        <div class="container">
            <div class="compare-top">
                <p>COMPARE OUR COURIERS</p>
            </div>
            <div class="total-compare">
                <div class="image-margin"><img src="{{asset('/images/logo1.png')}}"></div>
                <div class="image-margin"><img src="{{asset('/images/logo2.png')}}"></div>
                <div class="image-margin"><img src="{{asset('/images/logo3.png')}}"></div>
                <div class="image-margin"><img src="{{asset('/images/logo4.png')}}"></div>
                <div class="image-margin"><img src="{{asset('/images/logo5.png')}}"></div>
                <div class="image-margin"><img src="{{asset('/images/logo6.png')}}"></div>
                <div class="image-margin"><img src="{{asset('/images/logo8.png')}}"></div>
                <div><img src="{{asset('/images/logo7.png')}}"></div>
            </div>
        </div>
    </section>
    <!-- compare section end -->

@endsection
