<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Dashboard</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="{{asset ('/')}}css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="{{asset ('/')}}css/all.min.css">

    <!-- 3.owl carousel CSS -->
    <link rel="stylesheet" type="text/css" href="{{asset ('/')}}css/owl.carousel.min.css">

    <link rel="stylesheet" href="{{asset ('css/app.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset ('/')}}css/style.css">
    <link rel="stylesheet" href="{{asset ('css/dashboard.css')}}">
    @notifyCss
    <title>@yield('title',config('app.name'))</title>

</head>
<body style="background: #fff">

<header class="mb-3">
    <div class="top-header-bg">
        <div class="container">
            <div class="top-header">
                <div>
                    <img src="{{asset ('images/logo.png')}}" alt="">
                </div>
                <div>
                    <a href="{{route ('frontend.logout')}}">Logout</a>
                </div>
            </div>
        </div>
    </div>
    <div class="d-nav-bg shadow-sm">
        <div class="container">
            <nav class="d-nav">
                <ul>
                    <li><a href="#">Dashboard</a></li>
                    <li><a href="{{route ('frontend.shipping')}}">Add Shipping</a></li>
                    <li><a href="#">Pay Bill</a></li>
                    <li><a href="#">Profile</a></li>
                </ul>
            </nav>
        </div>
    </div>
</header>

<div class="container-fluid">
    @yield('main')
</div>
@include('frontend.partials.footer')
<script type="text/javascript" src="{{asset ('/')}}js/jquery-2.1.3.min.js"></script>
<script type="text/javascript" src="{{asset ('/')}}js/bootstrap.min.js"></script>
<script type="text/javascript" src="{{asset ('/')}}js/bootstrap.min.js"></script>
<script src="{{asset ('js/popper.min.js')}}"></script>
<script type="text/javascript" src="{{asset ('/')}}js/owl.carousel.min.js"></script>
<script type="text/javascript" src="{{asset ('/')}}js/main.js

"></script>
@include('notify::messages')
@notifyJs
@stack('js')
</body>
</html>