@extends('frontend.Layouts.app')
@section('title') Dashboard @endsection

@section('main')
    <div class="bg-white">
        <div class="container-fluid" style="background:#f5f5f5!important;">
            <div class="row py-5">
                <div class="col-2 pr-0">
                    <div class="card rounded-lg px-2 py-2 border-0 shadow-sm">
                        <div class="mt-3 mb-3 border-bottom">
                            <p>Hello <span class="text-primary font-weight-bold">,</span><span
                                    class="text-black-50 bold ml-1 text-capitalize">{{auth ()->user ()->account_type}}</span>
                            </p>
                        </div>
                        <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist"
                             aria-orientation="vertical">
                            <a class="nav-link active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home"
                               role="tab" aria-controls="v-pills-home" aria-selected="true">Dashboard</a>
                            <a class="nav-link" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile"
                               role="tab" aria-controls="v-pills-profile" aria-selected="false">Profile</a>
                            <a class="nav-link" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages"
                               role="tab" aria-controls="v-pills-messages" aria-selected="false">Send Request</a>
                            <a class="nav-link" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-settings"
                               role="tab" aria-controls="v-pills-settings" aria-selected="false">Recent Request</a>
                        </div>
                    </div>
                </div>
                <div class="col-10">
                    <div class="tab-content" id="v-pills-tabContent">
                        <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                        @include('frontend.partials.dashboard_panel')
                        </div>
                        <div class="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
                            @include('frontend.partials.profile')
                        </div>
                        <div class="tab-pane fade" id="v-pills-messages" role="tabpanel"
                             aria-labelledby="v-pills-messages-tab">
                            @include('frontend.partials.sendRequest')
                        </div>
                        <div class="tab-pane fade" id="v-pills-settings" role="tabpanel"
                             aria-labelledby="v-pills-settings-tab">
                            @include('frontend.partials.table')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
    @if($errors->any())
    <script !src="">
        $('#v-pills-messages-tab').tab('show');
    </script>
    @endif
@endpush
