@extends('backend.Layouts.app')

@section('main')
    <div class="row">
        <div class="col-xl-12">
            <div class="card spur-card rounded-lg">
                <div class="card-header d-flex justify-content-between">
                    <div class="spur-card-icon">
                        <i class="fas fa-chart-bar"></i>
                        @if($user->role=='admin')
                        <span class="spur-card-title">Admin User Edit</span>
                         @elseif($user->role=='employee')
                            <span class="spur-card-title">Employee Edit</span>
                        @elseif($user->role=='customer')
                            <span class="spur-card-title">Customer User Edit</span>
                        @endif
                    </div>
                </div>
                <div class="card-body ">
                    <form action="{{route('users.update',$user->id)}}" method="post">
                        @csrf
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="inputEmail4">Name</label>
                                <input name="name" type="text"
                                       class="form-control rounded-lg font-weight-bold @error('name') is-invalid @enderror"
                                       id="inputEmail4"
                                       placeholder="Name" value="{{old('name',$user->name)}}">
                                @error('name') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputPassword4">Username</label>
                                <input name="username" type="text"
                                       class="form-control rounded-lg font-weight-bold  @error('username') is-invalid @enderror"
                                       id="inputPassword4" placeholder="Username"
                                       value="{{old('username',$user->username)}}">
                                @error('username') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="inputEmail4">Email</label>
                                <input type="email" name="email"
                                       class="form-control rounded-lg font-weight-bold  @error('email') is-invalid @enderror"
                                       id="inputEmail4"
                                       placeholder="Email" value="{{old('email',$user->email)}}">
                                @error('email') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputEmail4">Phone</label>
                                <input type="tel" name="phone"
                                       class="form-control rounded-lg font-weight-bold  @error('phone') is-invalid @enderror"
                                       id="inputEmail4"
                                       placeholder="Phone number" value="{{old('phone',$user->phone)}}">
                                @error('phone') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="inputEmail4">Password</label>
                                <input type="password" name="password"
                                       class="form-control rounded-lg font-weight-bold  @error('password') is-invalid @enderror"
                                       id="inputPassword4" placeholder="Password">
                                @error('password') <span class="text-danger">{{$message}}</span> @enderror
                            </div>

                            <div class="form-group col-md-6">
                                <label for="inputPassword4"> Confirm Password</label>
                                <input type="password" name="password_confirmation"
                                       class="form-control rounded-lg font-weight-bold  @error('password_confirmation') is-invalid @enderror"
                                       id="inputPassword4" placeholder="Confirm Password">
                                @error('password_confirmation') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="inputState">Role</label>
                                <select id="inputState" name="role"
                                        class="form-control rounded-lg font-weight-bold  @error('role') is-invalid @enderror">
                                    @if($user->role=='admin')
                                        <option value="admin" selected>Admin</option>
                                    @elseif($user->role=='customer')
                                        <option value="customer" selected>Customer</option>
                                    @else
                                        <option value="employee" selected>Employee</option>
                                    @endif
                                </select>
                                @error('role') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputPassword4">Office</label>
                                <input type="text" name="office"
                                       class="form-control rounded-lg font-weight-bold  @error('office') is-invalid @enderror"
                                       id="inputPassword4" placeholder="Office" value="{{old('office',$user->office)}}">
                                @error('office') <span class="text-danger">{{$message}}</span> @enderror
                            </div>

                        </div>

                        <div class="form-group">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" {{$user->status===  true ? 'checked':''}} name="status"
                                       class="custom-control-input" id="customCheck4">
                                <label class="custom-control-label" for="customCheck4">Active</label>
                            </div>
                        </div>
                        <div>
                        <button type="submit" class="btn btn-info rounded-lg">
                            <i class="fa fa-save mr-1 "></i>Save
                        </button>

                            @if($user->role=='admin')
                                <a href="{{route('users.index')}}" class="bnt btn-warning text-decoration-none px-3 py-2 d-inline-block rounded-lg text-white font-weight-bold">
                                    <i class="fa fa-1x fa-arrow-circle-left mr-1"></i>
                                    Back
                                </a>
                            @elseif($user->role=='employee')
                                <a href="{{route('employee.list')}}" class="bnt btn-warning text-decoration-none px-3 py-2 d-inline-block rounded-lg text-white font-weight-bold">
                                    <i class="fa fa-1x fa-arrow-circle-left mr-1"></i>
                                    Back
                                </a>
                            @elseif($user->role=='customer')
                                <a href="{{route('customer.list')}}" class="bnt btn-warning text-decoration-none px-3 py-2 d-inline-block rounded-lg text-white font-weight-bold">
                                    <i class="fa fa-1x fa-arrow-circle-left mr-1"></i>
                                    Back
                                </a>
                            @endif
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
