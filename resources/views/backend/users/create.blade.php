@extends('backend.Layouts.app')


@section('main')
    <div class="row">
        <div class="col-xl-12">
            <div class="card spur-card rounded-lg">
                <div class="card-header d-flex justify-content-between">
                    <div class="spur-card-icon">
                        <i class="fas fa-chart-bar"></i>
                        <span class="spur-card-title">Administrator User</span>
                    </div>
                </div>
                <div class="card-body ">
                    <form action="{{route('users.store')}}" method="post">
                        @csrf
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="inputEmail4">Name <span
                                        class="text-danger font-weight-bold">*</span></label>
                                <input name="name" type="text"
                                       class="form-control rounded-lg font-weight-bold @error('name') is-invalid @enderror"
                                       id="inputEmail4"
                                       placeholder="Name" value="{{old('name')}}">
                                @error('name') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputPassword4">Username <span class="text-danger font-weight-bold">*</span></label>
                                <input name="username" type="text"
                                       class="form-control rounded-lg font-weight-bold  @error('username') is-invalid @enderror"
                                       id="inputPassword4" placeholder="Username"
                                       value="{{old('username')}}">
                                @error('username') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="inputEmail4">Email <span
                                        class="text-danger font-weight-bold">*</span></label>
                                <input type="email" name="email"
                                       class="form-control rounded-lg font-weight-bold  @error('email') is-invalid @enderror"
                                       id="inputEmail4"
                                       placeholder="Email" value="{{old('email')}}">
                                @error('email') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputEmail4">Phone <span
                                        class="text-danger font-weight-bold">*</span></label>
                                <input type="tel" name="phone"
                                       class="form-control rounded-lg font-weight-bold  @error('phone') is-invalid @enderror"
                                       id="inputEmail4"
                                       placeholder="Phone number" value="{{old('phone')}}">
                                @error('phone') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="inputEmail4">Password <span
                                        class="text-danger font-weight-bold">*</span></label>
                                <input type="password" name="password"
                                       class="form-control rounded-lg font-weight-bold  @error('password') is-invalid @enderror"
                                       id="inputPassword4" placeholder="Password">
                                @error('password') <span class="text-danger">{{$message}}</span> @enderror
                            </div>

                            <div class="form-group col-md-6">
                                <label for="inputPassword4"> Confirm Password <span
                                        class="text-danger font-weight-bold">*</span></label>
                                <input type="password" name="password_confirmation"
                                       class="form-control rounded-lg font-weight-bold  @error('password_confirmation') is-invalid @enderror"
                                       id="inputPassword4" placeholder="Confirm Password">
                                @error('password_confirmation') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="inputState">Role</label>
                                <select id="inputState" name="role"
                                        class="form-control rounded-lg font-weight-bold  @error('role') is-invalid @enderror">
                                    <option selected="" value="admin">Admin</option>
                                </select>
                                @error('role') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputPassword4">Office</label>
                                <input type="text" name="office"
                                       class="form-control rounded-lg font-weight-bold  @error('office') is-invalid @enderror"
                                       id="inputPassword4" placeholder="Office" value="{{old('office')}}">
                                @error('office') <span class="text-danger">{{$message}}</span> @enderror
                            </div>

                        </div>
                        <div class="form-group">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" checked name="status" class="custom-control-input"
                                       id="customCheck4">
                                <label class="custom-control-label" for="customCheck4">Active</label>
                            </div>
                        </div>
                        <div>
                            <button type="submit" class="btn btn-info rounded-lg">
                                <i class="fa fa-save mr-1 "></i>Save
                            </button>
                            <a href="{{route('users.index')}}"
                               class="bnt btn-warning text-decoration-none px-3 py-2 d-inline-block rounded-lg text-white font-weight-bold">
                                <i class="fa fa-1x fa-arrow-circle-left mr-1"></i>
                                Back
                            </a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
