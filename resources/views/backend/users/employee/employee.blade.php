@extends('backend.Layouts.app')

@section('main')
    <div class="card rounded-lg">
        <div class="card-header d-flex justify-content-between">
            <div class="text-dark font-weight-bold">
                Employee
            </div>
            <div>
                <a href="{{route('employee.create')}}" class="btn btn-info rounded-lg font-weight-bold">
                    <i class="fas fa-plus"></i>
                    Create
                </a>
            </div>
        </div>
        <div class="card-body">
            <table class="table table-bordered rounded">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Email</th>
                    <th scope="col">Phone</th>
                    <th scope="col">Office</th>
                    <th scope="col">Role</th>
                    <th scope="col">Status</th>
                    <th scope="col">Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $key=>$user)
                    <tr>
                        <th scope="row">{{$key+1}}</th>
                        <td>{{$user->name}}</td>
                        <td>{{$user->email}}</td>
                        <td>{{$user->phone}}</td>
                        <td>{{$user->office}}</td>
                        <td>{{$user->role}}</td>
                        <td>
                            @if($user->status===true)
                                <span class="badge badge-success p-1 rounded-lg">Active</span>
                            @else
                                <span class="badge badge-warning rounded-lg text-white p-1">Inactive</span>
                            @endif
                        </td>
                        <td>
                            <a href="{{route('users.delete',$user->id)}}" class="btn delete-confirm btn-danger rounded-lg">
                                <i class="fas fa-trash-alt"></i>
                            </a>
                            <a href="{{route('users.edit',$user->id)}}" class="btn btn-info rounded-lg">
                                <i class="fas fa-edit"></i>
                            </a>
                        </td>
                    </tr>
                </tbody>
                @endforeach
            </table>
        </div>
        <div class="card-footer text-right">
            {{$users->links()}}
        </div>
    </div>
@endsection
