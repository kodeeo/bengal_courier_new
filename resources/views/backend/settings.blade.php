<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Invoice</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body p-0">
                    <div class="row p-3">
                        <div class="col-md-6">
                            <img src="{{asset('images/logo.png')}}" alt="">
                        </div>

                        <div class="col-md-6 text-right">
                            <p class="font-weight-bold mb-1">Invoice #550</p>
                            <p class="text-muted">Due to: 4 Dec, 2019</p>
                        </div>
                    </div>

                    <hr class="my-5">

                    <div class="row pb-5 px-5">
                        <div class="col-md-4">
                            <p class="font-weight-bold mb-4">From</p>
                            <p class="mb-1 text-dark font-weight-bold">{{$order->sender_name}}</p>
                            <p><span>Phone</span>{{$order->sender_phone}}</p>
                            <p class="mb-1">
                                <span class="font-weight-bold text-dark">Address:</span>
                                {{$order->sender_address}}
                            </p>
                            <p class="mb-1">
                                Office:origin
                            </p>
                        </div>
                        <div class="col-md-4">
                            <p class="font-weight-bold mb-4">To</p>
                            <p class="mb-1 text-dark font-weight-bold">{{$order->receiver_name}}</p>
                            <p>Phone: {{$order->receiver_phone}}</p>
                            <p class="mb-1"><span class="text-dark">Address:</span>{{$order->receiver_address}}</p>
                            <p class="mb-1">Office:origin</p>
                        </div>
                        <div class="col-md-4 text-right">
                            <p class="font-weight-bold mb-4">
                                <img width="200px" height="50px"
                                     src="data:image/png;base64,{{DNS1D::getBarcodePNG($order->tracking_number, 'C93')}}"
                                     alt="barcode"/>
                                <span class="d-block mr-3">{{$order->tracking_number}}</span>
                            </p>
                            <p class="mb-1"><span
                                    class="text-muted font-weight-bold">ORDER ID:</span> {{$order->id}}</p>
                            <p class="mb-1"><span class="text-muted">Payment Deu: </span> 10253642</p>
                            <p class="mb-1"><span class="text-muted">Booking Mod: </span> {{$order->payment_status}}
                            </p>
                            <p class="mb-1"><span class="text-muted ">Name: </span> John Doe</p>
                        </div>
                    </div>

                    <div class="row p-5">
                        <div class="col-md-12">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th class="border-0 text-uppercase small font-weight-bold">ID</th>
                                    <th class="border-0 text-uppercase small font-weight-bold">Qty</th>
                                    <th class="border-0 text-uppercase small font-weight-bold">Product type</th>
                                    <th class="border-0 text-uppercase small font-weight-bold">Description</th>
                                    <th class="border-0 text-uppercase small font-weight-bold">Unit Cost</th>
                                    <th class="border-0 text-uppercase small font-weight-bold">sub total</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>{{$i=1}}</td>
                                    <td>{{(int)$order->quantity}}</td>
                                    <td>{{$order->productType->name}}</td>
                                    <td>{{$order->description}}</td>
                                    <td>{{$order->deliver_charge}}</td>
                                    <td>{{$order->deliver_charge}}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="d-flex flex-row-reverse  text-white p-4">
                        <div class="py-3 px-5 text-dark text-right">
                            <div class="mb-2">Grand Total</div>
                            <div class="h2 font-weight-light">{{$order->deliver_charge}}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>




