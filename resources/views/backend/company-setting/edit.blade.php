@extends('backend.Layouts.app')

@section('title') Setting update @endsection
@section('main')
    <div class="row">
        <div class="col-xl-12 rounded-lg">
            <div class="card spur-card rounded-lg border-0 shadow">
                <div class="card-header bg-white">
                    <div class="spur-card-icon">
                        <i class="flaticon2-settings"></i>
                    </div>
                    <div class="spur-card-title">Setting update</div>
                </div>
                <div class="card-body ">
                    <form action="{{route('companySettingUpdate',$setting->id)}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="company_name">Company name</label>
                                <input type="text" class="form-control @error('company_name') is-invalid @enderror rounded-lg" id="company_name"
                                       name="company_name" placeholder="company name" value="{{$setting->company_name}}">
                                @error('company_name')<span class="text-danger">{{$message}}</span> @enderror
                            </div>
                            <div class="form-group col-md-6">
                                <label for="company_phone">Company Phone</label>
                                <input type="text" class="form-control @error('company_phone') is-invalid @enderror rounded-lg" id="company_phone"
                                       name="company_phone" value="{{$setting->company_phone}}" placeholder="company phone number">
                                @error('company_phone')<span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="country">country</label>
                                <input type="text" class="form-control @error('country') is-invalid @enderror rounded-lg" name="country" id="country"
                                       placeholder="country" value="{{$setting->country}}">
                                @error('country')<span class="text-danger">{{$message}}</span> @enderror
                            </div>
                            <div class="form-group col-md-6">
                                <label for="email">Email</label>
                                <input type="email" class="form-control @error('email') is-invalid @enderror rounded-lg" name="email" id="per_kg_price"
                                       placeholder="email" value="{{$setting->email}}">
                                @error('email')<span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="city">city</label>
                                <input type="text" class="form-control @error('city') is-invalid @enderror rounded-lg" name="city" id="city"
                                       placeholder="city" value="{{$setting->city}}">
                                @error('city')<span class="text-danger">{{$message}}</span> @enderror
                            </div>
                            <div class="form-group col-md-4">

                                <label for="logo">Logo</label>
                                <input type="file" class="form-control pb-20 @error('logo') is-invalid @enderror rounded-lg" name="logo" id="logo"
                                       placeholder="Logo">
                            </div>
                            <div class="form-group col-md-2">
                                <img width="80px" height="80px" src="{{asset('upload/logo/'.$setting->logo)}}" alt="logo">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-12">
                                <label for="inputAddress">Address</label>
                                <textarea name="address" type="text" class="form-control rounded-lg" id="inputAddress"
                                          placeholder="1234 Main St">{{$setting->address}}</textarea>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-info rounded-lg float-right">
                            <i class="fa fa-save"></i>
                            setting update
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

