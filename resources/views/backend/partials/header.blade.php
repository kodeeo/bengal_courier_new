<header class="dash-toolbar shadow-sm" style="background:#f7fafc;">
    <a href="#!" class="menu-toggle">
        <i class="fas fa-bars"></i>
    </a>

    <div class="tools">
        {{--<a href="https://github.com/HackerThemes/spur-template" target="_blank" class="tools-item">--}}

        <a href="#!" class="tools-item">
            <i class="fas fa-bell"></i>
            <i class="tools-item-count"></i>
        </a>
        <div class="dropdown tools-item">
            <a href="#" class="" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-user"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-right" style="border: 1px solid #edf2f7;
    border-radius: 7px;" aria-labelledby="dropdownMenu1">
                <a class="dropdown-item" href="#!">{{auth()->user()->name}}</a>
                <a class="dropdown-item" href="{{route('logout')}}">Logout</a>
            </div>
        </div>
    </div>
</header>
