<nav class="dash-nav-list">
    <a href="{{route ('dashboard.index')}}" class="dash-nav-item">
        <i class="fas fa-home"></i> Dashboard </a>


    <div class="dash-nav-dropdown @if(request()->is('admin/users')|request()->is('admin/empolyee/list')|request()->is('admin/customer/list'))show @endif ">
        <a href="#!" class="dash-nav-item dash-nav-dropdown-toggle">
            <i class="fas fa-user"></i>Users</a>
        <div class="dash-nav-dropdown-menu">
            <a href="{{route('users.index')}}" class="dash-nav-dropdown-item">Administrator</a>
            <a href="{{route('employee.list')}}" class="dash-nav-dropdown-item">Employee</a>
            <a href="{{route('customer.list')}}" class="dash-nav-dropdown-item">Customers</a>
        </div>
    </div>

    <div class="dash-nav-dropdown {{request()->is('admin/product-types') ?' show':''}}">
        <a href="#!" class="dash-nav-item dash-nav-dropdown-toggle">
            <i class="fa fa-cogs"></i> Settings </a>
        <div class="dash-nav-dropdown-menu">
            <a href="{{route('productTypes.index')}}" class="dash-nav-dropdown-item {{request()->is('admin/product-types') ?' active':''}}">Products Type</a>
        </div>
    </div>

    <div class="dash-nav-dropdown  {{request()->is('admin/shipping-list')?' show':''}}">
        <a href="#!" class="dash-nav-item dash-nav-dropdown-toggle">
            <i class="fas fa-list-ul"></i> Parcel </a>
        <div class="dash-nav-dropdown-menu">
            <a href="{{route ('orders.index')}}" class="dash-nav-dropdown-item">Order List</a>
        </div>
    </div>
    <div class="dash-nav-dropdown {{request()->is('admin/online-order-lists')?' show':''}}">
        <a href="#!" class="dash-nav-item dash-nav-dropdown-toggle">
            <i class="fas fa-cube"></i> Online booking </a>
        <div class="dash-nav-dropdown-menu">
            <a href="{{route('orders.onlineOrderList')}}" class="dash-nav-dropdown-item">List</a>
        </div>
    </div>
    <div class="dash-nav-dropdown {{request()->is('admin/payment/*')?' show':''}}">
        <a href="#!" class="dash-nav-item dash-nav-dropdown-toggle">
            <i class="fas fa-book"></i> Reports </a>
        <div class="dash-nav-dropdown-menu">
            <a href="{{route('parcleReport')}}" class="dash-nav-dropdown-item">Parcel Report</a>
        </div>
    </div>

    <div class="dash-nav-dropdown {{request()->is('admin/company-settings')?' show':''}}">
        <a href="#!" class="dash-nav-item dash-nav-dropdown-toggle">
            <i class="fas fa-sliders-h"></i> Company Setting </a>
        <div class="dash-nav-dropdown-menu">
            <a href="{{route('companySetting')}}" class="dash-nav-dropdown-item">Setting</a>
        </div>
    </div>

</nav>
