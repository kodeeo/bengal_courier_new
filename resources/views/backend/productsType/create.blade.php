@extends('backend.Layouts.app')

@section('title') Product Type create @endsection

@section('main')

    <div class="card rounded-lg" xmlns="http://www.w3.org/1999/html">
        <div class="card-header">
            <h3>Product Type</h3>
        </div>
        <div class="card-body">
            <form action="{{route('productTypes.store')}}" method="post">
                @csrf
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <label for="name">Name of Product Type <span class="text-danger font-weight-bold">*</span></label>
                        <input type="text" class="form-control rounded-lg @error('name') is-invalid @enderror" id="name"
                               name="name"
                               placeholder="Name of Product Type">
                    </div>
                    <div class="form-group col-md-12">
                        <label for="type_of_package">Type of Packaging <span class="text-danger font-weight-bold">*</span></label>
                        <input type="text"
                               class="form-control rounded-lg @error('type_of_packing') is-invalid @enderror"
                               id="type_of_package"
                               placeholder="Type of Packaging" name="type_of_packing">
                    </div>
                    <div class="form-group col-md-12">
                        <label for="dimension">Dimension <span class="text-danger font-weight-bold">*</span></label>
                        <input type="text" class="form-control rounded-lg @error('dimension') is-invalid @enderror"
                               id="dimension" name="dimension"
                               placeholder="Dimension">
                    </div>
                </div>

                <div class="form-group">
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" checked name="status" class="custom-control-input" id="customCheck4">
                        <label class="custom-control-label" for="customCheck4">Active</label>
                    </div>
                </div>
               <div>
                   <button type="submit" class="btn btn-info rounded-lg">
                       <i class="fa fa-save mr-1 "></i>Save
                   </button>
                   <a href="{{route('productTypes.index')}}" class="btn btn-warning text-white rounded-lg">
                       <i class="fa fa-arrow-circle-left mr-1 "></i>Save
                   </a>

               </div>
            </form>
        </div>
    </div>

@endsection
