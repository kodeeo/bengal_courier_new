@extends('backend.Layouts.app')

@section('title') Product Type create @endsection

@section('main')

    <div class="card rounded-lg" xmlns="http://www.w3.org/1999/html">
        <div class="card-header">
            <h3>Product Type Edit</h3>
        </div>
        <div class="card-body">
            <form action="{{route('productTypes.update',$product_type->id)}}" method="post">
                @csrf
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <label for="name">Name of Product Type</label>
                        <input type="text" class="form-control rounded-lg @error('name') is-invalid @enderror" id="name"
                               name="name" value="{{old('name',$product_type->name)}}"
                               placeholder="Name of Product Type">
                    </div>
                    <div class="form-group col-md-12">
                        <label for="type_of_package">Type of Packaging</label>
                        <input type="text"
                               class="form-control rounded-lg @error('type_of_packing') is-invalid @enderror"
                               id="type_of_package"
                               placeholder="Type of Packaging" name="type_of_packing" value="{{old('type_of_packing',$product_type->type_of_packing)}}">
                    </div>
                    <div class="form-group col-md-12">
                        <label for="dimension">Dimension</label>
                        <input type="text" class="form-control rounded-lg @error('dimension') is-invalid @enderror"
                               id="dimension" name="dimension"
                               placeholder="Dimension" value="{{old('dimension',$product_type->dimension)}}">
                    </div>
                </div>

                <div class="form-group">
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" {{$product_type->status===1 ? 'checked' : ''}} name="status" class="custom-control-input" id="customCheck4">
                        <label class="custom-control-label" for="customCheck4">Active</label>
                    </div>
                </div>
                <button type="submit" class="btn btn-info rounded-lg">
                    <i class="fa fa-save mr-1 "></i>Save
                </button>
            </form>
        </div>
    </div>

@endsection
