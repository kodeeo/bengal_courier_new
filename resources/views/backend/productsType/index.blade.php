@extends('backend.Layouts.app')

@section('title') Product-Type @endsection

@section('main')
    <div class="card rounded-lg">
        <div class="card-header d-flex justify-content-between">
            <div class="text-dark display-5 font-weight-bold">
                Product Type
            </div>
            <div>
                <a href="{{route('productTypes.create')}}" class="btn btn-info rounded-lg font-weight-bold">
                    <i class="fas fa-plus"></i>
                    New Product type
                </a>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered rounded">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                        <th scope="col">Type of packing</th>
                        <th scope="col">Dimension</th>
                        <th scope="col">Status</th>
                        <th scope="col">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($product_types as $key=>$product_type)
                        <tr>
                            <th scope="row">{{$key+1}}</th>
                            <td>{{$product_type->name}}</td>
                            <td>{{$product_type->type_of_packing}}</td>
                            <td>{{$product_type->dimension}}</td>
                            <td>

                                @if($product_type->status===1)
                                    <span class="badge badge-success p-1 rounded-lg">Active</span>
                                @else
                                    <span class="badge badge-warning rounded-lg text-white p-1">Inactive</span>
                                @endif
                            </td>
                            <td class=" d-flex">
                                <a href="" class="btn delete-confirm btn-danger rounded-lg">
                                    <i class="fas fa-trash-alt"></i>
                                </a>
                                <a href="{{route('productTypes.edit',$product_type->id)}}" class="btn btn-info rounded-lg ml-2">
                                    <i class="fas fa-edit"></i>
                                </a>
                            </td>
                        </tr>
                    </tbody>
                    @endforeach
                </table>
            </div>
        </div>
        <div class="card-footer text-right">
            {{$product_types->links()}}
        </div>
    </div>
@endsection
