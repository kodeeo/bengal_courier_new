<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="">

    <title>Order Invoice</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/sticky-footer/">

    <!-- Bootstrap core CSS -->
    <link href="https://getbootstrap.com/docs/4.0/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="https://getbootstrap.com/docs/4.0/examples/sticky-footer/sticky-footer.css" rel="stylesheet">

    <style>
        #wrapper {
            max-width: 200px; /* max-width doesn't behave correctly in legacy IE */
            margin: 0 auto;

        }

        #wrapper img {
            width: 100%; /* the image will now scale down as its parent gets smaller */
        }

        ​
    </style>
</head>

<body>
<div class="container">
    <!-- Begin page content -->
                    <main role="main" class="container">
                        <section class="content content_content">
                            <section class="invoice">
                                <!-- title row -->
                                <div class="row" id="">
                                    <div class="col-md-4" id="">
                                        <h2 class="page-header">
                                            <img src="{{asset('images/logo.png')}}" alt="{{config('app.name')}}">
                                        </h2>

                                    </div><!-- /.col -->
                                    <div class="col-md-8" style="margin-left: 350px;">
                                        Tracking ID#<h6>{{$order->tracking_number}}</h6>
                                        <p class="font-weight-bold">
                                            <img
                                                src="data:image/png;base64,{{DNS1D::getBarcodePNG($order->tracking_number, 'C93')}}"
                                                alt="barcode"/>
                                        </p>

                                    </div>
                                </div>
                                <!-- info row -->

                                <hr>
                                <div class="row invoice-info">

                                    <div style="margin-right: 400px;">
                                        <u><b>Sender Info</b></u>
                                        <br>
                                        <b>Name :</b>
                                        <span>{{$order->sender_name}}</span>
                                        <br>
                                        <b>Email :</b>
                                        <span>{{$order->sender_email}}</span>
                                        <br>
                                        <b>Phone :</b>
                                        <span>{{$order->sender_phone}}</span>
                                        <br>
                                        <b>Address :</b>
                                        <span>{{$order->sender_address}}</span>


                                    </div>
                                    <div style="margin-left: 400px;">
                                        <u>Recipient Info</u>
                                        <br>
                                        <b>Name :</b>
                                        <span>{{$order->receiver_name}}</span>
                                        <br>
                                        <b>Phone :</b>
                                        <span>{{$order->receiver_phone}}</span> <br>
                                        <b>email :</b>
                                        <span>{{$order->receiver_email}}</span>

                                    </div>
                                </div><!-- /.row -->
                                <div class="row">
                                    <table class="table">
                                        <thead>
                                        <tr>

                                            <th scope="col">Quantity</th>
                                            <th scope="col">Product Type</th>
                                            <th scope="col">Unit Cost</th>
                                            <th scope="col">Subtotal</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>

                                            <td>{{(int)$order->quantity}}</td>
                                            <td>{{optional($order->productType)->name}}</td>
                                            <td>{{$order->deliver_charge}}</td>
                                            <td>{{$order->deliver_charge}}</td>
                                        </tr>
                                        <tr>
                                            <th>
                                                Description:
                                            </th>
                                            <td colspan="4">{{$order->description}}</td>
                                        </tr>
                                        <tr>
                                            <th>Payment Status</th>
                                            <td>{{$order->payment_status}}</td>
                                            <th>Delivery Date</th>
                                            <td>{{date('Y-m-d',strtotime($order->delivery_date_time))}}</td>
                                        </tr>
                                        </tbody>
                                    </table>


                                </div><!-- this row will not appear when printing -->
                            </section>
                        </section>
                    </main>
                    <footer class="footer" style="text-align: center;background: transparent">
                        <hr>
                        <div class="container">
                            <p class="text-muted">address ,Dhaka.</p>
                            <p>Email:info@bengalcourier.com Mobile:+88017 000 000</p>
                        </div>
                    </footer>
</div>
</body>
</html>



