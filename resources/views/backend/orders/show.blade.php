@extends('backend.Layouts.app')
@section('main')
    <div class="container">
        <form action="{{route('updateStatus',$order->id)}}" method="post">
            @csrf
            <div class="card border-0 shadow-sm rounded-lg">
                <div class="card-header d-flex justify-content-between">
                    <div>
                        <span>Order</span>
                    </div>
                    <div>
                        <a class="btn btn-warning rounded-lg d-inline-block text-white" href="{{route('orders.index')}}">Back</a>
                        <a href="{{route('invoice',$order->id)}}" class="btn btn-info rounded-lg px-5 text-uppercase">Print</a>
                        <button class="btn btn-info rounded-lg px-4 text-uppercase" type="submit">Update</button>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-9">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="card border-0 shadow-sm rounded-lg">
                                        <div class="card-header">
                                            Sender Details
                                        </div>
                                        <div class="card-body">
                                            <ul class="list-group list-unstyled">
                                                <li class="border-bottom py-1  px-2">
                                                    <span>Name :</span>
                                                    <span>{{$order->sender_name}}</span>
                                                </li>
                                                <li class="border-bottom py-1  px-2">
                                                    <span>Email :</span>
                                                    <span>{{$order->sender_email}}</span>
                                                </li>
                                                <li class="border-bottom py-1  px-2">
                                                    <span>Phone :</span>
                                                    <span>{{$order->sender_phone}}</span>
                                                </li>
                                                <li class="border-bottom py-1  px-2">
                                                    <span>Address :</span>
                                                    <span>{{$order->sender_address}}</span>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="card border-0 shadow-sm rounded-lg">
                                        <div class="card-header">
                                            Receiver Details
                                        </div>
                                        <div class="card-body">
                                            <ul class="list-group list-unstyled">
                                                <li class="border-bottom py-1  px-2">
                                                    <span>Name :</span>
                                                    <span>{{$order->receiver_name}}</span>
                                                </li>
                                                <li class="border-bottom py-1  px-2">
                                                    <span>Phone :</span>
                                                    <span>{{$order->receiver_phone}}</span>
                                                </li>
                                                <li class="border-bottom py-1  px-2">
                                                    <span>email :</span>
                                                    <span>{{$order->receiver_email}}</span>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12 pt-5">
                                    <div class="card rounded-lg border-0 shadow">
                                        <div class="card-header">
                                            <p>Product details</p>
                                        </div>
                                        <div class="card-body">
                                            <p>{{$order->description}}</p>
                                        </div>
                                        <div class="card-footer">
                                            Payment:
                                            <button type="button"
                                                    class="badge badge-info border-0 rounded-lg">{{$order->payment_status}}</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="card border-0 shadow-sm rounded-lg">
                                <div class="card-body">

                                    <div class="form-group">
                                        <label for="exampleFormControlSelect1">Status</label>
                                        <select class="form-control rounded-lg" name="status"
                                                id="exampleFormControlSelect1">
                                            <option value="pending" {{$order->status=== 'pending' ? 'selected':''}}>
                                                Pending
                                            </option>
                                            <option value="received" {{$order->status=== 'received' ? 'selected':''}}>
                                                Received
                                            </option>
                                            <option
                                                {{$order->status=== 'collecting' ? 'selected':''}} value="collecting ">
                                                Collecting
                                            </option>
                                            <option {{$order->status=== 'deliverd' ? 'selected':''}} value="deliverd">
                                                Delivered
                                            </option>
                                            <option
                                                {{$order->status=== 'redeliverd' ? 'selected':''}}  value="redeliverd">
                                                Re-Delivered
                                            </option>
                                            <option {{$order->status=== 'return' ? 'selected':''}}  value="return">
                                                Return
                                            </option>
                                        </select>
                                    </div>
                                    <div class="form-group d-flex">
                                        <div class="custom-control custom-radio mr-3">
                                            <input type="radio" id="paid" name="payment_status"
                                                   class="custom-control-input"
                                                   {{$order->payment_status==='paid' ? 'checked':''}} value="paid">
                                            <label class="custom-control-label" for="paid">Paid</label>
                                        </div>
                                        <div class="custom-control custom-radio">
                                            <input type="radio" id="cod" name="payment_status"
                                                   class="custom-control-input"
                                                   value="cod" {{$order->payment_status==='cod' ? 'checked':''}}>
                                            <label class="custom-control-label" for="cod">COD</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="shipping-charge">Shipping Charge</label>
                                        <input type="text" class="form-control rounded-lg" id="shipping-charge"
                                               placeholder="Shipping charge" value="{{number_format($order->deliver_charge)}}"
                                               name="deliver_charge">
                                    </div>
                                    <div class="form-group">
                                        <label for="shipping-charge">Delivery schedule</label>
                                        <input type="date" class="form-control rounded-lg" id="shipping-charge"

                                               placeholder="Shipping charge" value="{{$order->delivery_date}}"
                                               name="deliver_date">
                                    </div>
                                    <div class="form-group">
                                        <label for="shipping-charge">Collection schedule</label>
                                        <input  type="date" class="form-control rounded-lg" id="shipping-charge"
                                               placeholder="Shipping charge" value="{{$order->collect_date}}"
                                               name="collect_date">
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
