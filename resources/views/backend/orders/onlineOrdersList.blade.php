@extends('backend.Layouts.app')

@section('title') Product-Type @endsection

@section('main')
    <div class="card rounded-lg">
        <div class="card-header d-flex justify-content-between">
            <div>
                Online orders
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered rounded">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Tracking</th>
                        <th scope="col">pay mode</th>
                        <th scope="col">Shipper</th>
                        <th scope="col">Receiver</th>
                        <th scope="col">Date</th>
                        <th scope="col">Employee</th>
                        <th scope="col">status</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($orders as $key=>$order)
                        <tr>
                            <th scope="row">{{$key+1}}</th>
                            <td>{{$order->tracking_number}}</td>
                            <td>{{$order->payment_status}}</td>
                            <td>{{$order->sender_name}}</td>
                            <td>{{$order->receiver_name}}</td>
                            <td class="text-center">{{ date('d-m-Y', strtotime($order->delivery_date_time))}}</td>
                            <td>{{$order->receiver_name}}</td>
                            <td>
                                @if($order->status===true)
                                    <span class="badge badge-success p-1 rounded-lg">Active</span>
                                @else
                                    <span class="badge badge-warning rounded-lg text-white p-1">Inactive</span>
                                @endif
                            </td>
                            <td>
                                <a href="{{route('changeStatus',$order->id)}}"><i class="fa fa-edit"></i></a>
                            </td>
                        </tr>
                    </tbody>
                    @endforeach
                </table>
            </div>
        </div>
        <div class="card-footer text-right">
            {{$orders->links()}}
        </div>
    </div>
@endsection
