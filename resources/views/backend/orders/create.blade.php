@extends('backend.Layouts.app')
@section('main')
    <div class="card spur-card rounded-lg shadow overflow-auto">
        <div class="card-header bg-white">
            <div class="spur-card-icon">
                <i class="fas fa-chart-bar"></i>
            </div>
            <div class="spur-card-title">Shipping information</div>
        </div>
        <div class="card-body ">
            <form action="{{route('orders.store')}}" method="post">
                @csrf
                <div class="row">
                    <div class="col-lg-6 col-md-6">
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="sender">Sender</label>
                                <input type="text" class="form-control rounded-lg @error('sender_name') is-invalid @enderror" name="sender_name" id="sender"
                                       placeholder="Sender Name" value="{{old('sender_name')}}">
                                @error('sender_name') <span class="text-danger">{{$message}}</span>@enderror
                            </div>
                            <div class="form-group col-md-6">
                                <label for="phone">Sender Phone</label>
                                <input type="text" class="form-control rounded-lg @error('sender_phone') is-invalid @enderror" id="phone"
                                       placeholder="Phone number" name="sender_phone" value="{{old('sender_phone')}}">
                                @error('sender_phone') <span class="text-danger">{{$message}}</span>@enderror
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label for="sender">Address</label>
                                <textarea type="text" class="form-control rounded-lg @error('sender_address') is-invalid @enderror
                                    " id="sender"
                                          placeholder="Address" name="sender_address">{{old('sender_address')}}</textarea>
                                @error('sender_address') <span class="text-danger">{{$message}}</span>@enderror
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label for="phone">email</label>
                                <input type="text" class="form-control rounded-lg @error('sender_email') is-invalid @enderror" id="phone"
                                       placeholder="Sender email" name="sender_email" value="{{old('sender_email')}}">
                                @error('sender_email') <span class="text-danger">{{$message}}</span>@enderror
                            </div>
                        </div>
                        <div class="border-bottom mb-2 py-2">Shipping information</div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="inputState">Payment Mode</label>
                                <select id="inputState" name="payment_status" class="form-control rounded-lg">
                                    <option selected="" value="COD">COD</option>
                                    <option value="PAID">PAID</option>
                                </select>
                            </div>

                            <div class="form-group col-md-6">
                                <label for="inputState">Product/Type</label>
                                <select id="inputState" class="form-control rounded-lg @error('product_type') is-invalid @enderror" name="product_type">
                                    @foreach($product_types as $product_type)
                                        <option value="{{$product_type->id}}">{{$product_type->name}}</option>
                                    @endforeach
                                </select>
                                @error('product_type') <span class="text-danger">{{$message}}</span>@enderror
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="sender">Quantity</label>
                                <input type="number" class="form-control rounded-lg @error('quantity') is-invalid @enderror" id="sender"
                                       placeholder="0" name="quantity" value="{{old('quantity')}}">
                                @error('quantity') <span class="text-danger">{{$message}}</span>@enderror
                            </div>
                            <div class="form-group col-md-6">
                                <label for="sender">Weight <span>(kg)</span></label>
                                <input type="number" class="form-control rounded-lg  @error('weight') is-invalid @enderror" id="sender"
                                       placeholder="0" name="weight" value="{{old('weight')}}">
                                @error('weight') <span class="text-danger">{{$message}}</span>@enderror
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label for="sender">Details</label>
                                <textarea name="details" type="text" class="form-control rounded-lg" id="sender"
                                          placeholder="Product details">{{old('details')}}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="sender">Receiver Name</label>
                                <input name="receiver" type="text" class="form-control rounded-lg @error('receiver') is-invalid @enderror" id="sender"
                                       placeholder="Receiver Name" value="{{old('receiver')}}">
                                @error('receiver') <span class="text-danger">{{$message}}</span>@enderror
                            </div>
                            <div class="form-group col-md-6">
                                <label for="phone">Phone</label>
                                <input name="receiver_phone" type="text" class="form-control rounded-lg @error('receiver_phone') is-invalid @enderror" id="phone"
                                       placeholder="Receiver Phone number" value="{{old('receiver_phone')}}">
                                @error('receiver_phone') <span class="text-danger">{{$message}}</span>@enderror
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label for="sender">Address</label>
                                <textarea type="text" class="form-control rounded-lg @error('reciver_address') is-invalid @enderror" id="sender"
                                          placeholder="Receiver address" name="reciver_address">{{old('reciver_address')}}</textarea>
                                @error('reciver_address') <span class="text-danger">{{$message}}</span>@enderror
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label for="email">Email</label>
                                <input type="text" class="form-control rounded-lg @error('receiver_email') is-invalid @enderror" id="email"
                                       placeholder="Receiver email" name="receiver_email" value="{{old('receiver_email')}}">
                                @error('receiver_email') <span class="text-danger">{{$message}}</span>@enderror
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label for="phone">COLLECTION DATE AND TIME</label>
                                <input type="date" class="form-control rounded-lg" id="phone" name="collection_date">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <label for="inputState">Status</label>
                                <select  class="form-control rounded-lg" name="status"
                                        id="inputState">
                                    <option value="pending">
                                        PENDING
                                    </option>
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="phone">Schedule Delivery</label>
                                <input type="date" class="form-control rounded-lg" id="phone"
                                       placeholder="Phone number" name="delivery_date_time">
                            </div>
                        </div>


                    </div>
                </div>
                <button type="submit" class="btn btn-info rounded-lg float-right text-uppercase"><i
                        class="fa fa-save mr-2"></i>Save tracking
                </button>
            </form>
        </div>
    </div>
@endsection
