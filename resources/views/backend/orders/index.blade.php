@extends('backend.Layouts.app')

@section('title') Product-Type @endsection

@section('main')
    <div class="card rounded-lg">
        <div class="card-header d-flex justify-content-between">
            <div>
                Orders
            </div>
            <div>
                <a href="{{route('addShipping')}}" class="btn btn-info rounded-lg font-weight-bold">
                    <i class="fas fa-plus"></i>
                    New Shipping
                </a>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered rounded">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Tracking</th>
                        <th scope="col">pay mode</th>
                        <th scope="col">Shipper</th>
                        <th scope="col">Receiver</th>
                        <th scope="col">Date</th>
                        <th scope="col">Employee</th>
                        <th scope="col">status</th>
                        <th>action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($orders as $key=>$order)
                        <tr>
                            <th scope="row">{{$key+1}}</th>
                            <td>{{$order->tracking_number}}</td>
                            <td>{{$order->payment_status}}</td>
                            <td>{{$order->sender_name}}</td>
                            <td>{{$order->receiver_name}}</td>
                            <td>{{ date('d-m-Y', strtotime($order->delivery_date_time))}}</td>
                            <td>{{$order->receiver_name}}</td>
                            <td>
                                <span class="badge badge-info py-2 rounded-lg px-2 display-1">{{$order->status}}</span>
                            </td>
                            <td><a href="{{route('changeStatus',$order->id)}}"><i class="fa fa-edit"></i></a></td>
                        </tr>
                    </tbody>
                    @endforeach
                </table>
            </div>
        </div>
        <div class="card-footer text-right">
            {{$orders->links()}}
        </div>
    </div>
@endsection
