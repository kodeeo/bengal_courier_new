@extends('backend.Layouts.app')
@section('main')
    <div class="row dash-row">
        <div class="col-xl-4">
            <div class="stats stats-info rounded-lg shadow-sm">
                <h3 class="stats-title">Total Pending </h3>
                <div class="stats-content mb-3">
                    <div class="stats-icon">
                        <i class="fas fa-cubes"></i>
                    </div>
                    <div class="stats-data">
                        <div class="stats-number">{{$pending_order}}</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-4">
            <div class="stats stats-success rounded-lg shadow ">
                <h3 class="stats-title">Total Delivered </h3>
                <div class="stats-content">
                    <div class="stats-icon">
                        <i class="fas fa-truck"></i>
                    </div>
                    <div class="stats-data">
                        <div class="stats-number">{{$delivered_order}}</div>
                        <div class="stats-change">
                            <span class="stats-percentage"></span>
                            <span class="stats-timeframe"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-4">
            <div class="stats stats-warning rounded-lg">
                <h3 class="stats-title"> Total Received </h3>
                <div class="stats-content">
                    <div class="stats-icon">
                        <i class="fas fa-people-carry"></i>
                    </div>
                    <div class="stats-data">
                        <div class="stats-number">{{$recived_order}}</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-4">
            <div class="stats stats-dark rounded-lg">
                <h3 class="stats-title"> Total Return </h3>
                <div class="stats-content">
                    <div class="stats-icon">
                        <i class="fas fa-undo"></i>
                    </div>
                    <div class="stats-data">
                        <div class="stats-number">{{$return_order}}</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-4">
            <div class="stats stats-change rounded-lg">
                <h3 class="stats-title"> Total Merchant </h3>
                <div class="stats-content">
                    <div class="stats-icon">
                        <i class="fas fa-users"></i>
                    </div>
                    <div class="stats-data">
                        <div class="stats-number">5</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-4">
            <div class="stats stats-danger rounded-lg">
                <h3 class="stats-title"> Total Customer </h3>
                <div class="stats-content">
                    <div class="stats-icon">
                        <i class="fas fa-user-friends"></i>
                    </div>
                    <div class="stats-data">
                        <div class="stats-number">5</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


