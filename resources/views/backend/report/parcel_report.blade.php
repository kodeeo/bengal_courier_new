@extends('backend.Layouts.app')

@section('title') Product-Type @endsection

@section('main')
    <div class="card rounded-lg bg-white shadow border-0">
        <div class="card-header bg-white d-flex rounded-top align-items-center justify-content-between">
            <div class="text-black font-weight-bold">
                Parcel Report
            </div>
            <div>
                <form action="{{route('parcelShowreport')}}" method="get">
                    <div class="d-flex align-items-center">
                        <div class="ml-2">
                            <label for="form_date">Form</label>
                            <input type="date" name="form_date" class="form-control rounded-lg" id="form_date">
                        </div>
                        <div>
                            <label for="form_date">To</label>
                            <input type="date" name="to_date" class="form-control rounded-lg">
                        </div>
                        <div class="ml-2">
                            <label for="form_date">Status</label>
                            <select id="inputState" name="status" class="form-control rounded-lg font-weight-bold">
                                <option value="pending" selected>All</option>
                                <option value="pending" {{request('status')=='pending' ? 'selected':''}}>Pending</option>
                                <option value="received" {{request('status')=='received' ? 'selected':''}}>Received</option>
                                <option value="collecting" {{request('status')=='collecting' ? 'selected':''}}>Collecting</option>
                                <option value="deliverd" {{request('status')=='deliverd' ? 'selected':''}}>Delivered</option>
                                <option value="redeliverd" {{request('status')=='redeliverd' ? 'selected':''}}>Redelivered</option>
                                <option value="return" {{request('status')=='return' ? 'selected':''}}>Return</option>
                            </select>
                        </div>
                        <div class="ml-2">
                            <button type="submit" class="btn btn-success rounded-lg mt-4">Report</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive-lg table-responsive-md">
                <table class="table table-bordered rounded">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Tracking</th>
                        <th scope="col">Pay mode</th>
                        <th scope="col">Shipper</th>
                        <th scope="col">Receiver</th>
                        <th scope="col">Date</th>
                        <th scope="col">Employee</th>
                        <th scope="col">Amount</th>
                        <th scope="col">Status</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if($orders->count() !==0)
                        @foreach($orders as $key=>$order)
                            <tr>
                                <th scope="row">{{$key+1}}</th>
                                <td>{{$order->tracking_number}}</td>
                                <td>{{$order->payment_status}}</td>
                                <td>{{$order->sender_name}}</td>
                                <td>{{$order->receiver_name}}</td>
                                <td>{{ date('d-m-Y', strtotime($order->delivery_date_time))}}</td>
                                <td>{{$order->receiver_name}}</td>
                                <td>{{number_format($order->deliver_charge,2)}}</td>
                                <td>
                                    <span
                                        class="badge badge-info py-2 rounded-lg px-2 display-1">{{$order->status}}</span>
                                </td>
                                <td><a href="{{route('changeStatus',$order->id)}}"><i class="fa fa-edit"></i></a></td>
                            </tr>
                        @endforeach
                    @else
                        <tr class="d-table-row">
                            <td>
                                <div class="d-block bg-danger"> No found data</div>
                            </td>
                        </tr>
                    @endif
                    </tbody>

                </table>
            </div>
        </div>
        <div class="card-footer text-right">
            <span class="font-weight-bold">Total Amount:</span>
            <span>{{number_format($total)}}</span>
        </div>
    </div>
@endsection
