<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css"/>
    <link rel="stylesheet" href="{{asset('/css/app.css')}}"/>
    @notifyCss
    <title>@yield('title',config('app.name'))</title>
</head>

<body>
<div class="dash">
    <div class="dash-nav dash-nav-dark" style="background:#2d3748">
        <header>
            <img style="padding-bottom: 25px; width: 100px; height: auto; object-fit: cover" src="{{asset('upload/logo/'.setting('logo'))}}" alt="logo">
        </header>

        @include('backend.partials.nav')

    </div>

    <div class="dash-app bg-white">

        @include('backend.partials.header')

        <main class="dash-content">
            @yield('main')
        </main>

    </div>
</div>

{{--<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>--}}
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js"
    integrity="sha256-eGE6blurk5sHj+rmkfsGYeKyZx3M4bG+ZlFyA7Kns7E="
    crossorigin="anonymous"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="{{asset('/js/spur.js')}}"></script>
<script !src="">
    $('.delete-confirm').on('click', function (e) {
        event.preventDefault();
        const url = $(this).attr('href');
        swal({
            title: 'Are you sure?',
            text: 'This record and it`s details will be permanantly deleted!',
            icon: 'warning',
            buttons: ["Cancel", "Yes!"],
        }).then(function(value) {
            if (value) {
                window.location.href = url;
            }
        });
    });
  </script>


@include('notify::messages')
@notifyJs
@stack('js')
</body>

</html>
