<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::group(['namespace' => 'Frontend'], function () {
    Route::get('/', 'FrontendController@index')->name('home');
    Route::get('sing-up', 'AuthController@singUpFormShow')->name('sing-up');
    Route::get('login-form', 'AuthController@showLoginForm')->name('frontend.loginForm');
    Route::post('registration', 'AuthController@registrationProcess')
        ->name('frontend.registration');
    Route::post('login', 'AuthController@loginProcess')
        ->name('frontend.login');
    Route::get('/track-finds', 'ParcelTrackerController@track')->name('parcel.find');
    Route::get('/track-parcels', 'ParcelTrackerController@trackParcel')->name('parcel.track');
    Route::group(['middleware' => 'FrontendAuth'], static function () {
        Route::post('profile/update', 'AuthController@update')->name('profile.update');
        Route::post('password/change', 'AuthController@passwordChange')->name('passwordChange');
        Route::get('dashboard', 'DashboardController@index')->name('frontend.dashboard');
        Route::get('dashboard/shipping', 'DashboardController@shipping')->name('frontend.shipping');
        Route::get('logout', 'AuthController@logout')->name('frontend.logout');
        Route::post('send-request', 'OrderController@store')->name('frontend.request');
    });
});


Route::group(['prefix' => 'admin', 'namespace' => 'Backend'], static function () {
    Route::post('/login', 'AuthController@loginProcess')->name('login');
    Route::get('/', 'UsersController@login')->name('users.login');
    Route::group(['middleware' => ['auth','RoleCheck']], static function () {
        Route::get('dashboard', 'DashboardController@index')->name('dashboard.index');
        Route::get('/logout', 'AuthController@logout')->name('logout');
        Route::get('/users', 'UsersController@index')->name('users.index');
        Route::get('/users/edit/{id}', 'UsersController@edit')->name('users.edit');
        Route::post('/users/update/{id}', 'UsersController@update')->name('users.update');
        Route::get('/users/{id}/delete', 'UsersController@delete')->name('users.delete');
        Route::get('/users/create', 'UsersController@create')->name('users.create');
        Route::post('/users/store', 'UsersController@store')->name('users.store');
        Route::get('/empolyee/list', 'UsersController@empolyeList')->name('employee.list');
        Route::get('/empolyee/create', 'UsersController@employeeCreate')->name('employee.create');
        Route::post('/empolyee/create', 'UsersController@employeeStore')->name('employee.create');
        Route::get('/customer/list', 'UsersController@customerList')->name('customer.list');
        Route::get('/customer/create', 'UsersController@customerCreate')->name('customer.create');

        Route::get('product-types', 'ProductTypeController@index')->name('productTypes.index');
        Route::get('product-types/create', 'ProductTypeController@create')->name('productTypes.create');
        Route::post('product-types/store', 'ProductTypeController@store')->name('productTypes.store');
        Route::get('product-types/{id}/edit', 'ProductTypeController@edit')->name('productTypes.edit');
        Route::post('product-types/{id}/update', 'ProductTypeController@update')->name('productTypes.update');
        Route::get('shipping-list', 'OrderController@index')->name('orders.index');
        Route::get('chang-status/{id}', 'OrderController@show')->name('changeStatus');
        Route::post('update-status/{id}', 'OrderController@update')->name('updateStatus');
        Route::get('invoice/{id}', 'OrderController@printInvoice')->name('invoice');
        Route::get('online-order-lists', 'OrderController@onlineOrderList')->name('orders.onlineOrderList');
        Route::get('add-shipping', 'OrderController@create')->name('addShipping');
        Route::post('shipping-create', 'OrderController@store')->name('orders.store');
        Route::get('/company-settings', 'CompanySettingController@show')->name('companySetting');
        Route::post('/company-settings/{setting}', 'CompanySettingController@update')->name('companySettingUpdate');
        Route::get('/parcel-report','ReportController@index')->name('parcleReport');
        Route::get('/parcel-report/genearate','ReportController@generateReport')->name('parcelShowreport');

    });
});

//
//Route::fallback(static function () {
//    return redirect('/');
//});

